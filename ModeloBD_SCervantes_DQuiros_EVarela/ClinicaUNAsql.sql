﻿/*
Created: 23/8/2019
Modified: 22/9/2019
Model: ClinicaUNAModel
Database: Oracle 11g Release 1
*/


-- Create sequences section -------------------------------------------------

CREATE SEQUENCE CLINICAUNA.Cli_Usuario_seq01
 INCREMENT BY 1
 START WITH 1
 NOMAXVALUE
 NOCACHE
;

CREATE SEQUENCE CLINICAUNA.Cli_Medico_seq02
 INCREMENT BY 1
 START WITH 1
 NOMAXVALUE
 NOCACHE
;

CREATE SEQUENCE CLINICAUNA.Cli_Paciente_seq03
 INCREMENT BY 1
 START WITH 1
 NOMAXVALUE
 NOCACHE
;

CREATE SEQUENCE CLINICAUNA.Cli_Cita_seq04
 INCREMENT BY 1
 START WITH 1
 NOMAXVALUE
 NOCACHE
;

CREATE SEQUENCE CLINICAUNA.Cli_Expediente_seq06
 INCREMENT BY 1
 START WITH 1
 NOMAXVALUE
 NOCACHE
;

CREATE SEQUENCE CLINICAUNA.Cli_Archivos_seq07
 INCREMENT BY 1
 START WITH 1
 NOMAXVALUE
 NOCACHE
;

CREATE SEQUENCE CLINICAUNA.Cli_Especialidad_seq08
 INCREMENT BY 1
 START WITH 1
 NOMAXVALUE
 NOMINVALUE
 CACHE 20
;

-- Create tables section -------------------------------------------------

-- Table CLINICAUNA.Cli_Usuario

CREATE TABLE CLINICAUNA.Cli_Usuario(
  usu_id Number(10,0) NOT NULL,
  usu_nombre Varchar2(20 ) NOT NULL,
  usu_papellido Varchar2(20 ) NOT NULL,
  usu_sapellido Varchar2(20 ),
  usu_correo Varchar2(30 ) NOT NULL,
  usu_tipo Varchar2(1 ) NOT NULL,
  usu_idioma Varchar2(1 ),
  usu_contrasena Varchar2(20 ) NOT NULL,
  usu_usuario Varchar2(30 ) NOT NULL,
  usu_estado Varchar2(1 ) NOT NULL,
  usu_cedula Varchar2(20 ) NOT NULL,
  usu_version Number NOT NULL,
  CONSTRAINT ClinicaUNA_check_01 CHECK (usu_tipo in ('A', 'R', 'M')),
  CONSTRAINT ClinicaUNA_check_02 CHECK (usu_idioma in ('E', 'I')),
  CONSTRAINT ClinicaUNA_check_03 CHECK (usu_estado in ('A', 'I'))
)
;

-- Add keys for table CLINICAUNA.Cli_Usuario

ALTER TABLE CLINICAUNA.Cli_Usuario ADD CONSTRAINT PK_Cli_Usuario PRIMARY KEY (usu_id)
;

-- Table and Columns comments section

COMMENT ON TABLE CLINICAUNA.Cli_Usuario IS 'Usuarios de ingreso que va a contener la clinica'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Usuario.usu_id IS 'Id unico del usuario'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Usuario.usu_nombre IS 'Nombre de usuario'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Usuario.usu_papellido IS 'Primer apellido del usuario'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Usuario.usu_sapellido IS 'Segundo apellido del usuario '
;
COMMENT ON COLUMN CLINICAUNA.Cli_Usuario.usu_correo IS 'Correo electrinco del usuario '
;
COMMENT ON COLUMN CLINICAUNA.Cli_Usuario.usu_tipo IS 'Tipo de usuario (Administrador, Recepcionista, Medico)'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Usuario.usu_idioma IS 'Idioma a utilizar el sistema (Ingles, espanol)'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Usuario.usu_contrasena IS 'Contrasena de ingreso del usuario '
;
COMMENT ON COLUMN CLINICAUNA.Cli_Usuario.usu_usuario IS 'Nombre de usuario de ingreso al sistema '
;
COMMENT ON COLUMN CLINICAUNA.Cli_Usuario.usu_estado IS 'Estado del usuario (activo, inactivo)'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Usuario.usu_cedula IS 'Cedula del usuario'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Usuario.usu_version IS 'Version de registro del usuario '
;

-- Table CLINICAUNA.Cli_Medico

CREATE TABLE CLINICAUNA.Cli_Medico(
  med_id Number NOT NULL,
  med_codigo Varchar2(20 ) NOT NULL,
  med_folio Varchar2(30 ) NOT NULL,
  med_carne Varchar2(30 ) NOT NULL,
  med_inicioJornada Varchar2(30 ),
  med_finJordana Varchar2(30 ),
  med_version Number NOT NULL,
  med_espaciosPorHora Number NOT NULL,
  med_usu_id Number(10,0)
)
;

-- Add keys for table CLINICAUNA.Cli_Medico

ALTER TABLE CLINICAUNA.Cli_Medico ADD CONSTRAINT PK_Cli_Medico PRIMARY KEY (med_id)
;

-- Table and Columns comments section

COMMENT ON TABLE CLINICAUNA.Cli_Medico IS 'Medicos que va a contener la clinica'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Medico.med_id IS 'Id unico del medico'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Medico.med_codigo IS 'Codigo del medico'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Medico.med_folio IS 'Folio del medico'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Medico.med_carne IS 'Carné del medico'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Medico.med_inicioJornada IS 'Hora de inicio de jornada del medico'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Medico.med_finJordana IS 'Hora de fin de jornada del medico'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Medico.med_version IS 'Version del registro del medico'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Medico.med_espaciosPorHora IS 'Cantidad de espacios por atender por hora'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Medico.med_usu_id IS 'Usuario del medico'
;

-- Table CLINICAUNA.Cli_Paciente

CREATE TABLE CLINICAUNA.Cli_Paciente(
  pac_id Number NOT NULL,
  pac_cedula Varchar2(30 ) NOT NULL,
  pac_genero Varchar2(1 ),
  pac_fechaNacimiento Date,
  pac_nombre Varchar2(20 ),
  pac_apellidos Varchar2(30 ),
  pac_correo Varchar2(60 ) NOT NULL,
  pac_telefono Varchar2(30 ) NOT NULL,
  pac_exp_id Number,
  pac_version Number NOT NULL,
  CONSTRAINT ClinicaUNA_check04 CHECK (pac_genero in ('M', 'F'))
)
;

-- Add keys for table CLINICAUNA.Cli_Paciente

ALTER TABLE CLINICAUNA.Cli_Paciente ADD CONSTRAINT PK_Cli_Paciente PRIMARY KEY (pac_id)
;

-- Table and Columns comments section

COMMENT ON TABLE CLINICAUNA.Cli_Paciente IS 'Pacienes que se van a atender en la clinica'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Paciente.pac_id IS 'Id unico del paciente'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Paciente.pac_cedula IS 'Numero de cedula del paciente'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Paciente.pac_genero IS 'Genero del paciente'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Paciente.pac_fechaNacimiento IS 'Fecha de nacimiento del paciente'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Paciente.pac_nombre IS 'Nombre del paciente '
;
COMMENT ON COLUMN CLINICAUNA.Cli_Paciente.pac_apellidos IS 'Apellidos del paciente'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Paciente.pac_correo IS 'Direccion de correo del paciente'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Paciente.pac_telefono IS 'Numero de telefono del paciente'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Paciente.pac_exp_id IS 'Expediente del paciente'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Paciente.pac_version IS 'Version del registro paciente'
;

-- Table CLINICAUNA.Cli_Consulta

CREATE TABLE CLINICAUNA.Cli_Consulta(
  con_id Number NOT NULL,
  con_frecuenciaCardiaca Number,
  con_presion Number,
  con_peso Number,
  con_talla Number,
  con_temperatura Number,
  con_IMC Number,
  con_anotaciones Varchar2(200 ),
  con_apuntes Varchar2(200 ),
  con_planAtencion Varchar2(200 ),
  con_observaciones Varchar2(200 ),
  con_examenes Varchar2(200 ),
  con_tratamiento Varchar2(200 ),
  con_version Number NOT NULL,
  con_exp_id Number,
  con_cit_id Number
)
;

-- Add keys for table CLINICAUNA.Cli_Consulta

ALTER TABLE CLINICAUNA.Cli_Consulta ADD CONSTRAINT PK_Cli_Consulta PRIMARY KEY (con_id)
;

-- Table and Columns comments section

COMMENT ON TABLE CLINICAUNA.Cli_Consulta IS 'Citas agendadas en la clinica'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Consulta.con_id IS 'Id unico de la Consulta.'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Consulta.con_frecuenciaCardiaca IS 'Frecuencia cardiaca del paciente en el momento de la consulta'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Consulta.con_presion IS 'Presion del paciente en el momento de la consulta'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Consulta.con_peso IS 'Peso del paciente en el momento de la consulta'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Consulta.con_talla IS 'Talla del paciente en el momento de la consulta'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Consulta.con_temperatura IS 'Temperatura del paciente en el momento de la consulta'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Consulta.con_IMC IS 'Indice de masa corporal en el momento de la consulta'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Consulta.con_anotaciones IS 'Anotacines sobre la consulta'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Consulta.con_apuntes IS 'Apuntes hechos en la consulta'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Consulta.con_planAtencion IS 'Plan de atencion asignado al paciente en la consulta'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Consulta.con_observaciones IS 'Observaciones de la consulta'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Consulta.con_examenes IS 'Examenes asignados en la consulta'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Consulta.con_tratamiento IS 'Tratamiento asignado en la consulta'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Consulta.con_version IS 'Version del registro consulta'
;

-- Table CLINICAUNA.Cli_Expediente

CREATE TABLE CLINICAUNA.Cli_Expediente(
  exp_id Number NOT NULL,
  exp_antecedentesPatologicos Varchar2(200 ),
  exp_hospitalizaciones Varchar2(200 ),
  exp_operaciones Varchar2(200 ),
  exp_alergias Varchar2(200 ),
  exp_tratamientos Varchar2(200 ),
  exp_antFamiliar Varchar2(200 ),
  exp_version Number NOT NULL
)
;

-- Add keys for table CLINICAUNA.Cli_Expediente

ALTER TABLE CLINICAUNA.Cli_Expediente ADD CONSTRAINT PK_Cli_Expediente PRIMARY KEY (exp_id)
;

-- Table and Columns comments section

COMMENT ON TABLE CLINICAUNA.Cli_Expediente IS 'Expedientes de los pacientes de la clinica'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Expediente.exp_id IS 'Id unico del expediente'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Expediente.exp_antecedentesPatologicos IS 'Antecedetes que haya podido tener el paciente'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Expediente.exp_hospitalizaciones IS 'Hospitalizaciones pasadas'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Expediente.exp_operaciones IS 'Operaciones que posea el paciente'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Expediente.exp_alergias IS 'Alergias del paciente'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Expediente.exp_tratamientos IS 'Tratamientos que haya tomado el paciente'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Expediente.exp_antFamiliar IS 'Antecedentes familiares del paciente y su respectivo parentesco'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Expediente.exp_version IS 'Version del resgistro del expediente'
;

-- Table CLINICAUNA.Cli_Archivos

CREATE TABLE CLINICAUNA.Cli_Archivos(
  arc_id Number NOT NULL,
  arc_ruta Varchar2(300 ),
  arc_tipo Varchar2(50 ),
  arc_anotaciones Varchar2(200 ),
  arc_nombreExamen Varchar2(200 ),
  arc_fecha Date,
  arc_version Number NOT NULL,
  arc_exp_id Number
)
;

-- Add keys for table CLINICAUNA.Cli_Archivos

ALTER TABLE CLINICAUNA.Cli_Archivos ADD CONSTRAINT PK_Cli_Archivos PRIMARY KEY (arc_id)
;

-- Table and Columns comments section

COMMENT ON TABLE CLINICAUNA.Cli_Archivos IS 'Archivos contenidos dentro de los expedientes'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Archivos.arc_id IS 'Id unico del archivo'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Archivos.arc_ruta IS 'Ruta donde se encuentra guardado el archivo dentro del servidor'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Archivos.arc_tipo IS 'Tipo de archivo (Imagen, pdf, etc)'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Archivos.arc_anotaciones IS 'Anotaciones extras del medico sobre el archivo'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Archivos.arc_nombreExamen IS 'Nombre del examen al que puede pertenecer el archivo'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Archivos.arc_fecha IS 'Fecha del archivo'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Archivos.arc_version IS 'Version del registro del archivo'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Archivos.arc_exp_id IS 'Expediente al que pertenece'
;

-- Table CLINICAUNA.Cli_Especialidad

CREATE TABLE CLINICAUNA.Cli_Especialidad(
  esp_id Number NOT NULL,
  esp_nombre Varchar2(30 ),
  esp_med_id Number NOT NULL,
  esp_version Number
)
;

-- Add keys for table CLINICAUNA.Cli_Especialidad

ALTER TABLE CLINICAUNA.Cli_Especialidad ADD CONSTRAINT PK_Cli_Especialidad PRIMARY KEY (esp_id)
;

-- Table and Columns comments section

COMMENT ON TABLE CLINICAUNA.Cli_Especialidad IS 'Especialidades que pueden tener los medicos'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Especialidad.esp_id IS 'Id de la especialidad del medico'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Especialidad.esp_nombre IS 'Nombre de la especialidad asignada'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Especialidad.esp_version IS 'Version del registro'
;

-- Table CLINICAUNA.Cli_Cita

CREATE TABLE CLINICAUNA.Cli_Cita(
  cit_id Number NOT NULL,
  cit_estado Varchar2(1 ) NOT NULL,
  cit_telefono Varchar2(30 ) NOT NULL,
  cit_correo Varchar2(30 ) NOT NULL,
  cit_espacios Number NOT NULL,
  cit_fecha Date NOT NULL,
  cit_hora Varchar2(30 ) NOT NULL,
  cit_motivo Varchar2(100 ),
  cit_version Number,
  cit_med_id Number,
  CONSTRAINT ClinicaUNA_check05 CHECK (cit_estado in ('A', 'P', 'C', 'N'))
)
;

-- Add keys for table CLINICAUNA.Cli_Cita

ALTER TABLE CLINICAUNA.Cli_Cita ADD CONSTRAINT Cli_Cita_PK PRIMARY KEY (cit_id)
;

-- Table and Columns comments section

COMMENT ON COLUMN CLINICAUNA.Cli_Cita.cit_id IS 'Id de la cita'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Cita.cit_estado IS 'Estado de la cita programada, atendida, cancelada, ausente'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Cita.cit_telefono IS 'Telefono para confirmar la cita'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Cita.cit_correo IS 'Correo de confimacion para la cita'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Cita.cit_espacios IS 'Espacios solicitados para la cita'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Cita.cit_fecha IS 'Fecha de la cita'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Cita.cit_hora IS 'Hora de la cita'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Cita.cit_motivo IS 'Motivo de la cita'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Cita.cit_version IS 'Version del registro'
;
COMMENT ON COLUMN CLINICAUNA.Cli_Cita.cit_med_id IS 'Medico de la cita'
;

-- Trigger for sequence CLINICAUNA.Cli_Medico_seq02 for column med_id in table CLINICAUNA.Cli_Medico ---------
CREATE OR REPLACE TRIGGER CLINICAUNA.ts_Cli_Medico_Cli_Medico_seq02 BEFORE INSERT
ON CLINICAUNA.Cli_Medico FOR EACH ROW
BEGIN
if :new.med_id is null or :new.med_id<=0 then
  :new.med_id := CLINICAUNA.Cli_Medico_seq02.nextval;
end if;
END;
/
CREATE OR REPLACE TRIGGER CLINICAUNA.tsu_Cli_Medico_Cli_Medico_se_0 AFTER UPDATE OF med_id
ON CLINICAUNA.Cli_Medico FOR EACH ROW
BEGIN
  RAISE_APPLICATION_ERROR(-20010,'Cannot update column med_id in table CLINICAUNA.Cli_Medico as it uses sequence.');
END;
/

-- Trigger for sequence CLINICAUNA.Cli_Paciente_seq03 for column pac_id in table CLINICAUNA.Cli_Paciente ---------
CREATE OR REPLACE TRIGGER CLINICAUNA.ts_Cli_Paciente_Cli_Paciente_0 BEFORE INSERT
ON CLINICAUNA.Cli_Paciente FOR EACH ROW
BEGIN
if :new.pac_id is null or :new.pac_id <=0 then
  :new.pac_id := CLINICAUNA.Cli_Paciente_seq03.nextval;
end if;
END;
/
CREATE OR REPLACE TRIGGER CLINICAUNA.tsu_Cli_Paciente_Cli_Pacient_0 AFTER UPDATE OF pac_id
ON CLINICAUNA.Cli_Paciente FOR EACH ROW
BEGIN
  RAISE_APPLICATION_ERROR(-20010,'Cannot update column pac_id in table CLINICAUNA.Cli_Paciente as it uses sequence.');
END;
/

-- Trigger for sequence CLINICAUNA.Cli_Cita_seq04 for column con_id in table CLINICAUNA.Cli_Consulta ---------
CREATE OR REPLACE TRIGGER CLINICAUNA.ts_Cli_Consulta_Cli_Cita_seq04 BEFORE INSERT
ON CLINICAUNA.Cli_Consulta FOR EACH ROW
BEGIN
if :new.con_id is null or :new.con_id <=0 then
  :new.con_id := CLINICAUNA.Cli_Cita_seq04.nextval;
end if;
END;
/
CREATE OR REPLACE TRIGGER CLINICAUNA.tsu_Cli_Consulta_Cli_Cita_se_0 AFTER UPDATE OF con_id
ON CLINICAUNA.Cli_Consulta FOR EACH ROW
BEGIN
  RAISE_APPLICATION_ERROR(-20010,'Cannot update column con_id in table CLINICAUNA.Cli_Consulta as it uses sequence.');
END;
/

-- Trigger for sequence CLINICAUNA.Cli_Paciente_seq03 for column exp_id in table CLINICAUNA.Cli_Expediente ---------
CREATE OR REPLACE TRIGGER CLINICAUNA.ts_Cli_Expediente_Cli_Pacien_0 BEFORE INSERT
ON CLINICAUNA.Cli_Expediente FOR EACH ROW
BEGIN
if :new.exp_id is null or :new.exp_id <=0 then
  :new.exp_id := CLINICAUNA.Cli_Paciente_seq03.nextval;
end if;
END;
/
CREATE OR REPLACE TRIGGER CLINICAUNA.tsu_Cli_Expediente_Cli_Pacie_0 AFTER UPDATE OF exp_id
ON CLINICAUNA.Cli_Expediente FOR EACH ROW
BEGIN
  RAISE_APPLICATION_ERROR(-20010,'Cannot update column exp_id in table CLINICAUNA.Cli_Expediente as it uses sequence.');
END;
/

-- Trigger for sequence CLINICAUNA.Cli_Archivos_seq07 for column arc_id in table CLINICAUNA.Cli_Archivos ---------
CREATE OR REPLACE TRIGGER CLINICAUNA.ts_Cli_Archivos_Cli_Archivos_0 BEFORE INSERT
ON CLINICAUNA.Cli_Archivos FOR EACH ROW
BEGIN
if :new.arc_id is null or :new.arc_id <=0 then
  :new.arc_id := CLINICAUNA.Cli_Archivos_seq07.nextval;
end if;

END;
/
CREATE OR REPLACE TRIGGER CLINICAUNA.tsu_Cli_Archivos_Cli_Archivo_0 AFTER UPDATE OF arc_id
ON CLINICAUNA.Cli_Archivos FOR EACH ROW
BEGIN
  RAISE_APPLICATION_ERROR(-20010,'Cannot update column arc_id in table CLINICAUNA.Cli_Archivos as it uses sequence.');
END;
/

-- Trigger for sequence CLINICAUNA.Cli_Cita_seq04 for column cit_id in table CLINICAUNA.Cli_Cita ---------
CREATE OR REPLACE TRIGGER CLINICAUNA.ts_Cli_Cita_Cli_Cita_seq04 BEFORE INSERT
ON CLINICAUNA.Cli_Cita FOR EACH ROW
BEGIN
if :new.cit_id is null or :new.cit_id <=0 then
  :new.cit_id := CLINICAUNA.Cli_Cita_seq04.nextval;
end if;
END;
/
CREATE OR REPLACE TRIGGER CLINICAUNA.tsu_Cli_Cita_Cli_Cita_seq04 AFTER UPDATE OF cit_id
ON CLINICAUNA.Cli_Cita FOR EACH ROW
BEGIN
  RAISE_APPLICATION_ERROR(-20010,'Cannot update column cit_id in table CLINICAUNA.Cli_Cita as it uses sequence.');
END;
/


-- Create foreign keys (relationships) section ------------------------------------------------- 

ALTER TABLE CLINICAUNA.Cli_Medico ADD CONSTRAINT Cli_Usu_Med FOREIGN KEY (med_usu_id) REFERENCES CLINICAUNA.Cli_Usuario (usu_id)
;


ALTER TABLE CLINICAUNA.Cli_Paciente ADD CONSTRAINT Cli_Pac_Exp FOREIGN KEY (pac_exp_id) REFERENCES CLINICAUNA.Cli_Expediente (exp_id)
;


ALTER TABLE CLINICAUNA.Cli_Especialidad ADD CONSTRAINT Cli_Esp_Med FOREIGN KEY (esp_med_id) REFERENCES CLINICAUNA.Cli_Medico (med_id)
;


ALTER TABLE CLINICAUNA.Cli_Archivos ADD CONSTRAINT Cli_Exp_Arc FOREIGN KEY (arc_exp_id) REFERENCES CLINICAUNA.Cli_Expediente (exp_id)
;


ALTER TABLE CLINICAUNA.Cli_Cita ADD CONSTRAINT Cli_Med_Cit FOREIGN KEY (cit_med_id) REFERENCES CLINICAUNA.Cli_Medico (med_id)
;


ALTER TABLE CLINICAUNA.Cli_Consulta ADD CONSTRAINT Cli_Cit_Exp FOREIGN KEY (con_exp_id) REFERENCES CLINICAUNA.Cli_Expediente (exp_id)
;


ALTER TABLE CLINICAUNA.Cli_Consulta ADD CONSTRAINT Cli_Cit_Cons FOREIGN KEY (con_cit_id) REFERENCES CLINICAUNA.Cli_Cita (cit_id)
;




-- Grant permissions section -------------------------------------------------



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Dani
 */

@XmlRootElement (name = "UsuarioDto")
@XmlAccessorType (XmlAccessType.FIELD)

public class UsuarioDto 
{
   private Long usuId;
   private String usuNombre;
   private String usuPapellido;
   private String usuSapellido;
   private String usuCorreo;
   private String usuTipo;
   private String usuIdioma;
   private String usuContrasena;
   private String usuUsuario;
   private String usuEstado;
   private String usuCedula;
   
   public UsuarioDto()
   {
   }
   public UsuarioDto(Usuario usu) 
   {
        this.usuId=usu.getUsuId();
        this.usuNombre = usu.getUsuNombre();
        this.usuPapellido = usu.getUsuPapellido();
        this.usuSapellido = usu.getUsuSapellido();
        this.usuUsuario = usu.getUsuUsuario();
        this.usuContrasena = usu.getUsuContrasena();
        this.usuCedula = usu.getUsuCedula();
        this.usuCorreo = usu.getUsuCorreo();
        this.usuIdioma = usu.getUsuIdioma();
        this.usuEstado = usu.getUsuEstado();
        this.usuTipo = usu.getUsuTipo();
    }
   
    public Long getUsuId() 
    {
        return usuId;
    }

    public void setUsuId(Long usuId) 
    {
        this.usuId = usuId;
    }

    public String getUsuNombre() {
        return usuNombre;
    }

    public void setUsuNombre(String usuNombre) {
        this.usuNombre = usuNombre;
    }

    public String getUsuPapellido() {
        return usuPapellido;
    }

    public void setUsuPapellido(String usuPapellido) {
        this.usuPapellido = usuPapellido;
    }

    public String getUsuSapellido() {
        return usuSapellido;
    }

    public void setUsuSapellido(String usuSapellido) {
        this.usuSapellido = usuSapellido;
    }

    public String getUsuCorreo() {
        return usuCorreo;
    }

    public void setUsuCorreo(String usuCorreo) {
        this.usuCorreo = usuCorreo;
    }

    public String getUsuTipo() {
        return usuTipo;
    }

    public void setUsuTipo(String usuTipo) {
        this.usuTipo = usuTipo;
    }

    public String getUsuIdioma() {
        return usuIdioma;
    }

    public void setUsuIdioma(String usuIdioma) {
        this.usuIdioma = usuIdioma;
    }

    public String getUsuContrasena() {
        return usuContrasena;
    }

    public void setUsuContrasena(String usuContrasena) {
        this.usuContrasena = usuContrasena;
    }

    public String getUsuUsuario() {
        return usuUsuario;
    }

    public void setUsuUsuario(String usuUsuario) {
        this.usuUsuario = usuUsuario;
    }

    public String getUsuEstado() {
        return usuEstado;
    }

    public void setUsuEstado(String usuEstado) {
        this.usuEstado = usuEstado;
    }

    public String getUsuCedula() {
        return usuCedula;
    }

    public void setUsuCedula(String usuCedula) {
        this.usuCedula = usuCedula;
    }
}

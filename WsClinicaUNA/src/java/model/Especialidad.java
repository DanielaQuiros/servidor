/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Susana
 */
@Entity
@Table(name = "CLI_ESPECIALIDAD")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Especialidad.findAll", query = "SELECT e FROM Especialidad e") //SELECT c FROM Cita c join c.citMedId m WHERE m.medId = :idMedico
    , @NamedQuery(name = "Especialidad.findByEspId", query = "SELECT e FROM Especialidad e WHERE e.espId = :espId")
    , @NamedQuery(name = "Especialidad.findByEspNombre", query = "SELECT e FROM Especialidad e WHERE UPPER(e.espNombre) like :espNombre")
    , @NamedQuery(name="Especialidad.findByEspMed",query="SELECT e FROM Especialidad e LEFT JOIN e.medicoList m where e.espNombre like '%'")
        
})
public class Especialidad implements Serializable {


    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
     @SequenceGenerator(name = "CLI_ESPECIALIDAD_EXP_ID_GENERATOR", sequenceName = "CLINICAUNA.CLI_ESPECIALIDAD_SEQ08", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CLI_ESPECIALIDAD_EXP_ID_GENERATOR")
    @Basic(optional = false)
    @Column(name = "ESP_ID")
    private Long espId; 
    @Column(name = "ESP_NOMBRE")
    private String espNombre;
    @Version
    @Column(name = "ESP_VERSION")
    private Integer espVersion;
    @OneToMany(mappedBy = "medEspId") //en el medico no hay un atributo de tipo especialidad ?                                               
    private List<Medico> medicoList;

    public Especialidad() {
    }

    public Especialidad(Long espId) {
        this.espId = espId;
    }
    public void actualizar(EspecialidadDto esp)
    {
       // this.espId = esp.getEspId();
        this.espNombre = esp.getEspNombre();
    }
    public Especialidad(EspecialidadDto esp)
    {
        this.espId = esp.getEspId();
        actualizar(esp);
    }
    public Long getEspId() {
        return espId;
    }

    public void setEspId(Long espId) {
        this.espId = espId;
    }

    public String getEspNombre() {
        return espNombre;
    }

    public void setEspNombre(String espNombre) {
        this.espNombre = espNombre;
    }

    public Integer getEspVersion() {
        return espVersion;
    }

    public void setEspVersion(Integer espVersion) {
        this.espVersion = espVersion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (espId != null ? espId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Especialidad)) {
            return false;
        }
        Especialidad other = (Especialidad) object;
        if ((this.espId == null && other.espId != null) || (this.espId != null && !this.espId.equals(other.espId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Especialidad[ espId=" + espId + " ]";
    }

    @XmlTransient
    public List<Medico> getMedicoList() {
        return medicoList;
    }

    public void setMedicoList(List<Medico> medicoList) {
        this.medicoList = medicoList;
    }
    
}

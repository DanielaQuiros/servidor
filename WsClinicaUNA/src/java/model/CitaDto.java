/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.time.LocalDate;
import java.time.ZoneId;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import util.LocalDateAdapter;

/**
 *
 * @author liedu
 */
@XmlRootElement(name = "CitaDto")
@XmlAccessorType(XmlAccessType.FIELD)
public class CitaDto {

    private Long citId;
    private String citEstado;
    private String citTelefono;
    private String citCorreo;
    private Integer citEspacios;
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    private LocalDate citFecha;
    private String citHora;
    private String citMotivo;;
//    List<Expediente> iExpedienteList;
    private MedicoDto citMedId;
    private PacienteDto paciente; 
   public CitaDto() {
    }

    public CitaDto(Cita cit) {
        this.citId = cit.getCitId();
        this.citEstado = cit.getCitEstado();
        this.citTelefono = cit.getCitTelefono();
        this.citCorreo = cit.getCitCorreo();
        this.citEspacios = cit.getCitEspacios();
        this.citFecha = cit.getCitFecha().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        this.citHora = cit.getCitHora();
        this.citMotivo = cit.getCitMotivo();
        

    }

    public Long getCitId() {
        return citId;
    }

    public void setCitId(Long citId) {
        this.citId = citId;
    }

    public String getCitEstado() {
        return citEstado;
    }

    public void setCitEstado(String citEstado) {
        this.citEstado = citEstado;
    }

    public String getCitTelefono() {
        return citTelefono;
    }

    public void setCitTelefono(String citTelefono) {
        this.citTelefono = citTelefono;
    }

    public String getCitCorreo() {
        return citCorreo;
    }

    public void setCitCorreo(String citCorreo) {
        this.citCorreo = citCorreo;
    }

    public Integer getCitEspacios() {
        return citEspacios;
    }

    public void setCitEspacios(Integer citEspacios) {
        this.citEspacios = citEspacios;
    }

    public LocalDate getCitFecha() {
        return citFecha;
    }

    public void setCitFecha(LocalDate citFecha) {
        this.citFecha = citFecha;
    }

    public String getCitHora() {
        return citHora;
    }

    public void setCitHora(String citHora) {
        this.citHora = citHora;
    }

    public String getCitMotivo() {
        return citMotivo;
    }

    public void setCitMotivo(String citMotivo) {
        this.citMotivo = citMotivo;
    }

    public MedicoDto getCitMedId() {
        return citMedId;
    }

    public void setCitMedId(MedicoDto citMedId) {
        this.citMedId = citMedId;
    }

    public PacienteDto getPaciente() {
        return paciente;
    }

    public void setPaciente(PacienteDto paciente) {
        this.paciente = paciente;
    }

}

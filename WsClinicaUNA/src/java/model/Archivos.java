/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.time.ZoneId;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Susana
 */
@Entity
@Table(name = "CLI_ARCHIVOS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Archivos.findAll", query = "SELECT a FROM Archivos a")
    , @NamedQuery(name = "Archivos.findByArcId", query = "SELECT a FROM Archivos a WHERE a.arcId = :arcId")
    , @NamedQuery(name = "Archivos.findByArcRuta", query = "SELECT a FROM Archivos a WHERE a.arcRuta = :arcRuta")
    , @NamedQuery(name = "Archivos.findByArcTipo", query = "SELECT a FROM Archivos a WHERE a.arcTipo = :arcTipo")
    , @NamedQuery(name = "Archivos.findByArcAnotaciones", query = "SELECT a FROM Archivos a WHERE a.arcAnotaciones = :arcAnotaciones")
    , @NamedQuery(name = "Archivos.findByArcNombreexamen", query = "SELECT a FROM Archivos a WHERE a.arcNombreexamen = :arcNombreexamen")
    , @NamedQuery(name = "Archivos.findByArcFecha", query = "SELECT a FROM Archivos a WHERE a.arcFecha = :arcFecha")
    , @NamedQuery(name = "Archivos.findByExpId", query = "SELECT a FROM Archivos a WHERE a.arcExpId = :arcExpId", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))})
public class Archivos implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "CLI_ARCHIVOS_ID_GENERATOR", sequenceName = "CLINICAUNA.CLI_ARCHIVOS_SEQ07", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CLI_ARCHIVOS_ID_GENERATOR")
    @Basic(optional = false)
    @Column(name = "ARC_ID")
    private Long arcId;
    @Column(name = "ARC_RUTA")
    private String arcRuta;
    @Column(name = "ARC_TIPO")
    private String arcTipo;
    @Column(name = "ARC_ANOTACIONES")
    private String arcAnotaciones;
    @Column(name = "ARC_NOMBREEXAMEN")
    private String arcNombreexamen;
    @Column(name = "ARC_FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date arcFecha;
    @Version
    @Column(name = "ARC_VERSION")
    private Long arcVersion;
    @JoinColumn(name = "ARC_EXP_ID", referencedColumnName = "EXP_ID")
    @ManyToOne
    private Expediente arcExpId;

    public Archivos() {
    }

    public Archivos(ArchivoDto archivo) {
        if(this.arcId != null)
            this.arcId = archivo.getId();
        actualizar(archivo);
    }
    
    public void actualizar(ArchivoDto archivo){
        this.arcRuta = archivo.getRuta();
        this.arcTipo = archivo.getTipo();
        this.arcAnotaciones = archivo.getAnotaciones();
        this.arcNombreexamen = archivo.getNombreExamen();
        this.arcFecha = Date.from(archivo.getFecha().atStartOfDay(ZoneId.systemDefault()).toInstant());
        if(archivo.getExpediente() != null)
            this.arcExpId = new Expediente(archivo.getExpediente());
    }

    public Archivos(Long arcId) {
        this.arcId = arcId;
    }

    public Archivos(Long arcId, Long arcVersion) {
        this.arcId = arcId;
        this.arcVersion = arcVersion;
    }

    public Long getArcId() {
        return arcId;
    }

    public void setArcId(Long arcId) {
        this.arcId = arcId;
    }

    public String getArcRuta() {
        return arcRuta;
    }

    public void setArcRuta(String arcRuta) {
        this.arcRuta = arcRuta;
    }

    public String getArcTipo() {
        return arcTipo;
    }

    public void setArcTipo(String arcTipo) {
        this.arcTipo = arcTipo;
    }

    public String getArcAnotaciones() {
        return arcAnotaciones;
    }

    public void setArcAnotaciones(String arcAnotaciones) {
        this.arcAnotaciones = arcAnotaciones;
    }

    public String getArcNombreexamen() {
        return arcNombreexamen;
    }

    public void setArcNombreexamen(String arcNombreexamen) {
        this.arcNombreexamen = arcNombreexamen;
    }

    public Date getArcFecha() {
        return arcFecha;
    }

    public void setArcFecha(Date arcFecha) {
        this.arcFecha = arcFecha;
    }

    public Long getArcVersion() {
        return arcVersion;
    }

    public void setArcVersion(Long arcVersion) {
        this.arcVersion = arcVersion;
    }

    public Expediente getArcExpId() {
        return arcExpId;
    }

    public void setArcExpId(Expediente arcExpId) {
        this.arcExpId = arcExpId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (arcId != null ? arcId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Archivos)) {
            return false;
        }
        Archivos other = (Archivos) object;
        if ((this.arcId == null && other.arcId != null) || (this.arcId != null && !this.arcId.equals(other.arcId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Archivos[ arcId=" + arcId + " ]";
    }
    
}

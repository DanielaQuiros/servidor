/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.QueryHint;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Susana
 */
@Entity
@Table(name = "CLI_MEDICO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Medico.findAll", query = "SELECT m FROM Medico m")
    , @NamedQuery(name = "Medico.findByMedId", query = "SELECT m FROM Medico m WHERE m.medId = :medId")
    , @NamedQuery(name = "Medico.findByFolioCarneCod", query = "SELECT m FROM Medico m WHERE UPPER(m.medFolio) like :folio and UPPER(m.medCarne) like :carne and UPPER(m.medCodigo) like :codigo ", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "Medico.findByEspecialidad", query = "Select m From Medico m join m.medEspId e where e.espId = :EspId", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "Medico.findByUsuario", query = "Select m From Medico m join m.medUsuId e where e.usuId = :medUsuId", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "Medico.findAtendidas", query = "SELECT count(c.citId) FROM Medico m join m.citaList c where m.medId= :medId and c.citEstado ='A'", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "Medico.findAusentes", query = "SELECT count(c.citId) FROM Medico m join m.citaList c where m.medId= :medId and c.citEstado ='N'", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "Medico.findCanceladas", query = "SELECT count(c.citId) FROM Medico m join m.citaList c where m.medId= :medId and c.citEstado ='C'", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "Medico.findProgramadas", query = "SELECT count(c.citId) FROM Medico m join m.citaList c where m.medId= :medId and c.citEstado ='P'", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))

})

public class Medico implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "CLI_MEDICO_MED_ID_GENERATOR", sequenceName = "CLINICAUNA.CLI_MEDICO_SEQ02", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CLI_MEDICO_MED_ID_GENERATOR")
    @Basic(optional = false)
    @Column(name = "MED_ID")
    private Long medId;
    @Basic(optional = false)
    @Column(name = "MED_CODIGO")
    private String medCodigo;
    @Basic(optional = false)
    @Column(name = "MED_FOLIO")
    private String medFolio;
    @Basic(optional = false)
    @Column(name = "MED_CARNE")
    private String medCarne;
    @Column(name = "MED_INICIOJORNADA")
    private String medIniciojornada;
    @Column(name = "MED_FINJORDANA")
    private String medFinjordana;
    @OneToMany(mappedBy = "citMedId")
    private List<Cita> citaList;
    @JoinColumn(name = "MED_USU_ID", referencedColumnName = "USU_ID")
    @ManyToOne
    private Usuario medUsuId;
    @Version
    @Column(name = "MED_VERSION")
    private Integer medVersion;
    @Basic(optional = false)
    @Column(name = "MED_ESPACIOSPORHORA")
    private Integer medEspaciosporhora;
    @JoinColumn(name = "MED_ESP_ID", referencedColumnName = "ESP_ID")
    @ManyToOne
    private Especialidad medEspId;

    public Medico() {
    }

    public Medico(Long medId) {
        this.medId = medId;
    }

    public Medico(Long medId, String medCodigo, String medFolio, String medCarne, Integer medVersion, Integer medEspaciosporhora) {
        this.medId = medId;
        this.medCodigo = medCodigo;
        this.medFolio = medFolio;
        this.medCarne = medCarne;
        this.medVersion = medVersion;
        this.medEspaciosporhora = medEspaciosporhora;
    }

    public void actualizar(MedicoDto medico) {
        this.medCarne = medico.getMedCarne();
        this.medCodigo = medico.getMedCodigo();
        this.medIniciojornada = medico.getMedIniciojornada();
        this.medFinjordana = medico.getMedFinjornada();
        this.medEspaciosporhora = medico.getMedEspaciosPorHora();
        this.medFolio = medico.getMedFolio();
        this.medUsuId = new Usuario(medico.getMedUsuId());
        this.medEspId = new Especialidad(medico.getMedEspId());
    }

    public Medico(MedicoDto medico) {
        this.medId = medico.getMedId();
        actualizar(medico);
    }

    public Long getMedId() {
        return medId;
    }

    public void setMedId(Long medId) {
        this.medId = medId;
    }

    public String getMedCodigo() {
        return medCodigo;
    }

    public void setMedCodigo(String medCodigo) {
        this.medCodigo = medCodigo;
    }

    public String getMedFolio() {
        return medFolio;
    }

    public void setMedFolio(String medFolio) {
        this.medFolio = medFolio;
    }

    public String getMedCarne() {
        return medCarne;
    }

    public void setMedCarne(String medCarne) {
        this.medCarne = medCarne;
    }

    public String getMedIniciojornada() {
        return medIniciojornada;
    }

    public void setMedIniciojornada(String medIniciojornada) {
        this.medIniciojornada = medIniciojornada;
    }

    public String getMedFinjordana() {
        return medFinjordana;
    }

    public void setMedFinjordana(String medFinjordana) {
        this.medFinjordana = medFinjordana;
    }

    public Integer getMedVersion() {
        return medVersion;
    }

    public void setMedVersion(Integer medVersion) {
        this.medVersion = medVersion;
    }

    public Integer getMedEspaciosporhora() {
        return medEspaciosporhora;
    }

    public void setMedEspaciosporhora(Integer medEspaciosporhora) {
        this.medEspaciosporhora = medEspaciosporhora;
    }

    @XmlTransient
    public List<Cita> getCitaList() {
        return citaList;
    }

    public void setCitaList(List<Cita> citaList) {
        this.citaList = citaList;
    }

    public Usuario getMedUsuId() {
        return medUsuId;
    }

    public void setMedUsuId(Usuario medUsuId) {
        this.medUsuId = medUsuId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (medId != null ? medId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Medico)) {
            return false;
        }
        Medico other = (Medico) object;
        if ((this.medId == null && other.medId != null) || (this.medId != null && !this.medId.equals(other.medId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Medico[ medId=" + medId + " ]";
    }

    public Especialidad getMedEspId() {
        return medEspId;
    }

    public void setMedEspId(Especialidad medEspId) {
        this.medEspId = medEspId;
    }

}

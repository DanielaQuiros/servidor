/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.QueryHint;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Susana
 */
@Entity
@Table(name = "CLI_CITA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cita.findAll", query = "SELECT c FROM Cita c")
    , @NamedQuery(name = "Cita.findByCitId", query = "SELECT c FROM Cita c WHERE c.citId = :citId")
    , @NamedQuery(name = "Cita.findByMedico", query = "SELECT c FROM Cita c join c.citMedId m WHERE m.medId = :idMedico", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "Cita.findByMedicoCita", query = "SELECT c FROM Cita c join c.citMedId m WHERE m.medId = :idMedico  and c.citFecha = :fechaCita", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "Cita.findByMedPacFec", query = "SELECT c FROM Cita c join c.citMedId m join c.citPacId p WHERE Upper(m.medUsuId.usuNombre) like Upper(:medName) and Upper(p.pacNombre) like Upper(:paciName) and c.citFecha = :fecha", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
   

})
public class Cita implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "CLI_CITA_EXP_ID_GENERATOR", sequenceName = "CLINICAUNA.CLI_CITA_SEQ04", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CLI_CITA_EXP_ID_GENERATOR")
    @Basic(optional = false)
    @Column(name = "CIT_ID")
    private Long citId;
    @Basic(optional = false)
    @Column(name = "CIT_ESTADO")
    private String citEstado;
    @Basic(optional = false)
    @Column(name = "CIT_TELEFONO")
    private String citTelefono;
    @Basic(optional = false)
    @Column(name = "CIT_CORREO")
    private String citCorreo;
    @Basic(optional = false)
    @Column(name = "CIT_FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date citFecha;
    @Basic(optional = false)
    @Column(name = "CIT_HORA")
    private String citHora;
    @Column(name = "CIT_MOTIVO")
    private String citMotivo;
    @Basic(optional = false)
    @Column(name = "CIT_ESPACIOS")
    private Integer citEspacios;
    @Version
    @Column(name = "CIT_VERSION")
    private Integer citVersion;
    @JoinColumn(name = "CIT_PAC_ID", referencedColumnName = "PAC_ID")
    @ManyToOne
    private Paciente citPacId;
    @JoinColumn(name = "CIT_MED_ID", referencedColumnName = "MED_ID")
    @ManyToOne
    private Medico citMedId;
    @OneToMany(mappedBy = "conCitId")
    private List<Consulta> consultaList;

    public Cita() {
    }

    public Cita(Long citId) {
        this.citId = citId;
    }

    public Cita(Long citId, String citEstado, String citTelefono, String citCorreo, Integer citEspacios, Date citFecha, String citHora) {
        this.citId = citId;
        this.citEstado = citEstado;
        this.citTelefono = citTelefono;
        this.citCorreo = citCorreo;
        this.citEspacios = citEspacios;
        this.citFecha = citFecha;
        this.citHora = citHora;
    }

    public void actualizar(CitaDto cita) {
        this.citEstado = cita.getCitEstado();
        this.citCorreo = cita.getCitCorreo();
        this.citEspacios = cita.getCitEspacios();
        this.citMotivo = cita.getCitMotivo();
        this.citFecha = Date.from(cita.getCitFecha().atStartOfDay(ZoneId.systemDefault()).toInstant());
        this.citHora = cita.getCitHora();
        this.citTelefono = cita.getCitTelefono();
    }

    public Cita(CitaDto cita) {
        this.citId = cita.getCitId();
        actualizar(cita);
    }

    public Long getCitId() {
        return citId;
    }

    public void setCitId(Long citId) {
        this.citId = citId;
    }

    public String getCitEstado() {
        return citEstado;
    }

    public void setCitEstado(String citEstado) {
        this.citEstado = citEstado;
    }

    public String getCitTelefono() {
        return citTelefono;
    }

    public void setCitTelefono(String citTelefono) {
        this.citTelefono = citTelefono;
    }

    public String getCitCorreo() {
        return citCorreo;
    }

    public void setCitCorreo(String citCorreo) {
        this.citCorreo = citCorreo;
    }

    public Integer getCitEspacios() {
        return citEspacios;
    }

    public void setCitEspacios(Integer citEspacios) {
        this.citEspacios = citEspacios;
    }

    public Date getCitFecha() {
        return citFecha;
    }

    public void setCitFecha(Date citFecha) {
        this.citFecha = citFecha;
    }

    public String getCitHora() {
        return citHora;
    }

    public void setCitHora(String citHora) {
        this.citHora = citHora;
    }

    public String getCitMotivo() {
        return citMotivo;
    }

    public void setCitMotivo(String citMotivo) {
        this.citMotivo = citMotivo;
    }

    public Integer getCitVersion() {
        return citVersion;
    }

    public void setCitVersion(Integer citVersion) {
        this.citVersion = citVersion;
    }

    public Medico getCitMedId() {
        return citMedId;
    }

    public void setCitMedId(Medico citMedId) {
        this.citMedId = citMedId;
    }

    @XmlTransient
    public List<Consulta> getConsultaList() {
        return consultaList;
    }

    public void setConsultaList(List<Consulta> consultaList) {
        this.consultaList = consultaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (citId != null ? citId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cita)) {
            return false;
        }
        Cita other = (Cita) object;
        if ((this.citId == null && other.citId != null) || (this.citId != null && !this.citId.equals(other.citId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Cita[ citId=" + citId + " ]";
    }

    public Paciente getCitPacId() {
        return citPacId;
    }

    public void setCitPacId(Paciente citPacId) {
        this.citPacId = citPacId;
    }

}

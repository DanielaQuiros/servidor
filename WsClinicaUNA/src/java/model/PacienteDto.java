/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.time.LocalDate;
import java.time.ZoneId;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import util.LocalDateAdapter;

/**
 *
 * @author Susana
 */
@XmlRootElement(name = "PacienteDto")
@XmlAccessorType(XmlAccessType.FIELD)
public class PacienteDto {

    private Long pacId;
    private String pacCedula;
    private String pacGenero;
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    private LocalDate pacFechaNacimiento;
    private String pacApellidos;
    private String pacNombre;
    private String pacCorreo;
    private String pacTelefono;
    private Integer pacVersion;

    public PacienteDto() {

    }

    public PacienteDto(Paciente paciente) {
        this.pacId = paciente.getPacId();
        this.pacCedula = paciente.getPacCedula();
        this.pacGenero = paciente.getPacGenero();
        this.pacApellidos = paciente.getPacApellidos();
        if (paciente.getPacFechanacimiento() != null) {
            this.pacFechaNacimiento = paciente.getPacFechanacimiento().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        }
        this.pacNombre = paciente.getPacNombre();
        this.pacCorreo = paciente.getPacCorreo();
        this.pacTelefono = paciente.getPacTelefono();
    }

    public void actualizar(PacienteDto pacienteDto) {
        this.pacId = pacienteDto.getPacId();
        this.pacCedula = pacienteDto.getPacCedula();
        this.pacCorreo = pacienteDto.getPacCorreo();
        this.pacTelefono = pacienteDto.getPacTelefono();
    }

    public Long getPacId() {
        return pacId;
    }

    public void setPacId(Long pacId) {
        this.pacId = pacId;
    }

    public String getPacCedula() {
        return pacCedula;
    }

    public void setPacCedula(String pacCedula) {
        this.pacCedula = pacCedula;
    }

    public String getPacGenero() {
        return pacGenero;
    }

    public void setPacGenero(String pacGenero) {
        this.pacGenero = pacGenero;
    }

    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    public LocalDate getPacFechaNacimiento() {
        return pacFechaNacimiento;
    }

    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    public void setPacFechaNacimiento(LocalDate pacFechaNacimiento) {
        this.pacFechaNacimiento = pacFechaNacimiento;
    }

    public String getPacApellidos() {
        return pacApellidos;
    }

    public void setPacApellidos(String pacApellidos) {
        this.pacApellidos = pacApellidos;
    }

    public String getPacNombre() {
        return pacNombre;
    }

    public void setPacNombre(String pacNombre) {
        this.pacNombre = pacNombre;
    }

    public String getPacCorreo() {
        return pacCorreo;
    }

    public void setPacCorreo(String pacCorreo) {
        this.pacCorreo = pacCorreo;
    }

    public String getPacTelefono() {
        return pacTelefono;
    }

    public void setPacTelefono(String PacTelefono) {
        this.pacTelefono = PacTelefono;
    }

    public Integer getPacVersion() {
        return pacVersion;
    }

    public void setPacVersion(Integer pacVersion) {
        this.pacVersion = pacVersion;
    }

}

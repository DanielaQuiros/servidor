/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.QueryHint;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Susana
 */
@Entity
@Table(name = "CLI_PACIENTE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Paciente.findAll", query = "SELECT p FROM Paciente p")
    , @NamedQuery(name = "Paciente.findByPacId", query = "SELECT p FROM Paciente p WHERE p.pacId = :pacId")
    , @NamedQuery(name = "Paciente.findByPacCedula", query = "SELECT p FROM Paciente p WHERE p.pacCedula = :pacCedula")
    , @NamedQuery(name = "Paciente.findByPacGenero", query = "SELECT p FROM Paciente p WHERE p.pacGenero = :pacGenero")
    , @NamedQuery(name = "Paciente.findByPacFechanacimiento", query = "SELECT p FROM Paciente p WHERE p.pacFechanacimiento = :pacFechanacimiento")
    , @NamedQuery(name = "Paciente.findByPacNombre", query = "SELECT p FROM Paciente p WHERE p.pacNombre = :pacNombre")
    , @NamedQuery(name = "Paciente.findByPacApellidos", query = "SELECT p FROM Paciente p WHERE p.pacApellidos = :pacApellidos")
    , @NamedQuery(name = "Paciente.findByPacCorreo", query = "SELECT p FROM Paciente p WHERE p.pacCorreo = :pacCorreo")
    , @NamedQuery(name = "Paciente.findByPacTelefono", query = "SELECT p FROM Paciente p WHERE p.pacTelefono = :pacTelefono")
    , @NamedQuery(name = "Paciente.findByPacVersion", query = "SELECT p FROM Paciente p WHERE p.pacVersion = :pacVersion")
    , @NamedQuery(name = "Paciente.findByCedulaNombreApellidos", query = "SELECT p FROM Paciente p WHERE (p.pacNombre) like :nombre and (p.pacCedula) like :cedula and (p.pacApellidos) like :apellidos", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
})
public class Paciente implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "CLI_PACIENTE_ID_GENERATOR", sequenceName = "CLINICAUNA.CLI_PACIENTE_SEQ03", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CLI_PACIENTE_ID_GENERATOR")
    @Basic(optional = false)
    @Column(name = "PAC_ID")
    private Long pacId;
    @Basic(optional = false)
    @Column(name = "PAC_CEDULA")
    private String pacCedula;
    @Column(name = "PAC_GENERO")
    private String pacGenero;
    @Column(name = "PAC_FECHANACIMIENTO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date pacFechanacimiento;
    @Column(name = "PAC_NOMBRE")
    private String pacNombre;
    @Column(name = "PAC_APELLIDOS")
    private String pacApellidos;
    @Basic(optional = false)
    @Column(name = "PAC_CORREO")
    private String pacCorreo;
    @Basic(optional = false)
    @Column(name = "PAC_TELEFONO")
    private String pacTelefono;
    @Version
    @Column(name = "PAC_VERSION")
    private Integer pacVersion;
    @OneToMany(mappedBy = "citPacId")
    private List<Cita> citaList;

    @JoinColumn(name = "PAC_EXP_ID", referencedColumnName = "EXP_ID")
    @ManyToOne
    private Expediente pacExpId;

    public Paciente() {
    }

    public Paciente(Long pacId) {
        this.pacId = pacId;
    }

    public Paciente(Long pacId, String pacCedula, String pacCorreo, String pacTelefono, Integer pacVersion) {
        this.pacId = pacId;
        this.pacCedula = pacCedula;
        this.pacCorreo = pacCorreo;
        this.pacTelefono = pacTelefono;
        this.pacVersion = pacVersion;
    }

    public Paciente(PacienteDto pacienteDto) {
        this.pacId = pacienteDto.getPacId();
        this.pacCedula = pacienteDto.getPacCedula();
        this.pacGenero = pacienteDto.getPacGenero();
        this.pacFechanacimiento = Date.from(pacienteDto.getPacFechaNacimiento().atStartOfDay(ZoneId.systemDefault()).toInstant());
        this.pacNombre = pacienteDto.getPacNombre();
        this.pacApellidos = pacienteDto.getPacApellidos();
        this.pacCorreo = pacienteDto.getPacCorreo();
        this.pacTelefono = pacienteDto.getPacTelefono();
        //this.pacExpId = 
    }

    public void actualizar(PacienteDto pacienteDto) {
        this.pacId = pacienteDto.getPacId();
        this.pacCedula = pacienteDto.getPacCedula();
        this.pacCorreo = pacienteDto.getPacCorreo();
        this.pacTelefono = pacienteDto.getPacTelefono();
        this.pacNombre = pacienteDto.getPacNombre();
        this.pacApellidos = pacienteDto.getPacApellidos();
        this.pacGenero = pacienteDto.getPacGenero();
        this.pacFechanacimiento = Date.from(pacienteDto.getPacFechaNacimiento().atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    public Long getPacId() {
        return pacId;
    }

    public void setPacId(Long pacId) {
        this.pacId = pacId;
    }

    public String getPacCedula() {
        return pacCedula;
    }

    public void setPacCedula(String pacCedula) {
        this.pacCedula = pacCedula;
    }

    public String getPacGenero() {
        return pacGenero;
    }

    public void setPacGenero(String pacGenero) {
        this.pacGenero = pacGenero;
    }

    public Date getPacFechanacimiento() {
        return pacFechanacimiento;
    }

    public void setPacFechanacimiento(Date pacFechanacimiento) {
        this.pacFechanacimiento = pacFechanacimiento;
    }

    public String getPacNombre() {
        return pacNombre;
    }

    public void setPacNombre(String pacNombre) {
        this.pacNombre = pacNombre;
    }

    public String getPacApellidos() {
        return pacApellidos;
    }

    public void setPacApellidos(String pacApellidos) {
        this.pacApellidos = pacApellidos;
    }

    public String getPacCorreo() {
        return pacCorreo;
    }

    public void setPacCorreo(String pacCorreo) {
        this.pacCorreo = pacCorreo;
    }

    public String getPacTelefono() {
        return pacTelefono;
    }

    public void setPacTelefono(String pacTelefono) {
        this.pacTelefono = pacTelefono;
    }

    public Integer getPacVersion() {
        return pacVersion;
    }

    public void setPacVersion(Integer pacVersion) {
        this.pacVersion = pacVersion;
    }

    public Expediente getPacExpId() {
        return pacExpId;
    }

    public void setPacExpId(Expediente pacExpId) {
        this.pacExpId = pacExpId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pacId != null ? pacId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Paciente)) {
            return false;
        }
        Paciente other = (Paciente) object;
        if ((this.pacId == null && other.pacId != null) || (this.pacId != null && !this.pacId.equals(other.pacId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Paciente[ pacId=" + pacId + " ]";
    }

    @XmlTransient
    public List<Cita> getCitaList() {
        return citaList;
    }

    public void setCitaList(List<Cita> citaList) {
        this.citaList = citaList;
    }

}

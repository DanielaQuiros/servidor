/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Susana
 */
@Entity
@Table(name = "CLI_CONSULTA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Consulta.findAll", query = "SELECT c FROM Consulta c")
    , @NamedQuery(name = "Consulta.findByConId", query = "SELECT c FROM Consulta c WHERE c.conId = :conId")
    , @NamedQuery(name ="Consulta.findByCita" , query = "SELECT c FROM Consulta c join c.conCitId t WHERE t.citId = :citId",hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name ="Consulta.findByExpediente" , query = "SELECT c FROM Consulta c join c.conExpId x  where x.expId = :expId",hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
     })
public class Consulta implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "CLI_CONSULTA_EXP_ID_GENERATOR", sequenceName = "CLINICAUNA.CLI_CONSULTA_SEQ05", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CLI_CONSULTA_EXP_ID_GENERATOR")
    @Basic(optional = false)
    @Column(name = "CON_ID")
    private Long conId;
    @Column(name = "CON_FRECUENCIACARDIACA")
    private Integer conFrecuenciacardiaca;
    @Column(name = "CON_PRESION")
    private Integer conPresion;
    @Column(name = "CON_PESO")
    private Long conPeso;
    @Column(name = "CON_TALLA")
    private Integer conTalla;
    @Column(name = "CON_TEMPERATURA")
    private Integer conTemperatura;
    @Column(name = "CON_IMC")
    private Long conImc;
    @Column(name = "CON_ANOTACIONES")
    private String conAnotaciones;
    @Column(name = "CON_APUNTES")
    private String conApuntes;
    @Column(name = "CON_PLANATENCION")
    private String conPlanatencion;
    @Column(name = "CON_OBSERVACIONES")
    private String conObservaciones;
    @Column(name = "CON_EXAMENES")
    private String conExamenes;
    @Column(name = "CON_TRATAMIENTO")
    private String conTratamiento;
    @Version
    @Basic(optional = false)
    @Column(name = "CON_VERSION")
    private Integer conVersion;
    @JoinColumn(name = "CON_CIT_ID", referencedColumnName = "CIT_ID")
    @ManyToOne
    private Cita conCitId;
    @JoinColumn(name = "CON_EXP_ID", referencedColumnName = "EXP_ID")
    @ManyToOne
    private Expediente conExpId;

    public Consulta() {
    }
    public void actualizar(ConsultaDto consulta)
    {
        this.conFrecuenciacardiaca = consulta.getConFrecCardiaca();//ConFrecCardiaca
        this.conPresion = consulta.getConPresion();
        this.conPeso = consulta.getConPeso();
        this.conTalla = consulta.getConTalla();
        this.conTemperatura = consulta.getConTemperatura();
        this.conImc = consulta.getConImc();
        this.conAnotaciones = consulta.getConAnotaciones();
        this.conApuntes = consulta.getConApuntes();
        this.conPlanatencion = consulta.getConPlanatencion();
        this.conObservaciones = consulta.getConObservaciones();
        this.conExamenes = consulta.getConExamenes();
        this.conTratamiento = consulta.getConTratamiento();
        this.conCitId = new Cita(consulta.getCita());
        this.conExpId = new Expediente(consulta.getExpediente());
    }
    public Consulta(ConsultaDto con) 
    {
       this.conId = con.getConId();
       actualizar(con);
    }
    
    public Consulta(Long conId,Integer conVersion) {
        this.conId = conId;
        this.conVersion = conVersion;
    }

    public Long getConId() {
        return conId;
    }

    public void setConId(Long conId) {
        this.conId = conId;
    }

    public Integer getConFrecuenciacardiaca() {
        return conFrecuenciacardiaca;
    }

    public void setConFrecuenciacardiaca(Integer conFrecuenciacardiaca) {
        this.conFrecuenciacardiaca = conFrecuenciacardiaca;
    }

    public Integer getConPresion() {
        return conPresion;
    }

    public void setConPresion(Integer conPresion) {
        this.conPresion = conPresion;
    }

    public Long getConPeso() {
        return conPeso;
    }

    public void setConPeso(Long conPeso) {
        this.conPeso = conPeso;
    }

    public Integer getConTalla() {
        return conTalla;
    }

    public void setConTalla(Integer conTalla) {
        this.conTalla = conTalla;
    }

    public Integer getConTemperatura() {
        return conTemperatura;
    }

    public void setConTemperatura(Integer conTemperatura) {
        this.conTemperatura = conTemperatura;
    }

    public Long getConImc() {
        return conImc;
    }

    public void setConImc(Long conImc) {
        this.conImc = conImc;
    }

    public String getConAnotaciones() {
        return conAnotaciones;
    }

    public void setConAnotaciones(String conAnotaciones) {
        this.conAnotaciones = conAnotaciones;
    }

    public String getConApuntes() {
        return conApuntes;
    }

    public void setConApuntes(String conApuntes) {
        this.conApuntes = conApuntes;
    }

    public String getConPlanatencion() {
        return conPlanatencion;
    }

    public void setConPlanatencion(String conPlanatencion) {
        this.conPlanatencion = conPlanatencion;
    }

    public String getConObservaciones() {
        return conObservaciones;
    }

    public void setConObservaciones(String conObservaciones) {
        this.conObservaciones = conObservaciones;
    }

    public String getConExamenes() {
        return conExamenes;
    }

    public void setConExamenes(String conExamenes) {
        this.conExamenes = conExamenes;
    }

    public String getConTratamiento() {
        return conTratamiento;
    }

    public void setConTratamiento(String conTratamiento) {
        this.conTratamiento = conTratamiento;
    }

    public Integer getConVersion() {
        return conVersion;
    }

    public void setConVersion(Integer conVersion) {
        this.conVersion = conVersion;
    }

    public Cita getConCitId() {
        return conCitId;
    }

    public void setConCitId(Cita conCitId) {
        this.conCitId = conCitId;
    }

    public Expediente getConExpId() {
        return conExpId;
    }

    public void setConExpId(Expediente conExpId) {
        this.conExpId = conExpId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (conId != null ? conId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Consulta)) {
            return false;
        }
        Consulta other = (Consulta) object;
        if ((this.conId == null && other.conId != null) || (this.conId != null && !this.conId.equals(other.conId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Consulta[ conId=" + conId + " ]";
    }

}

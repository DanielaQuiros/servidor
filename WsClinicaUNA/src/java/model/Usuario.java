/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.QueryHint;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author liedu
 */
@Entity
@Table(name = "CLI_USUARIO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Usuario.findAll", query = "SELECT u FROM Usuario u")
    , @NamedQuery(name = "Usuario.findByUsuId", query = "SELECT u FROM Usuario u WHERE u.usuId = :usuId")
    , @NamedQuery(name = "Usuario.findByUsuUsuario" , query = "SELECT u FROM Usuario u WHERE u.usuUsuario = :usuUsuario")
    , @NamedQuery(name = "Usuario.findByUsuCorreo", query = "SELECT u FROM Usuario u WHERE u.usuCorreo = :usuCorreo")
    , @NamedQuery(name = "Usuario.findByUsuarioContrasena", query = "SELECT u FROM Usuario u WHERE u.usuUsuario = :usuUsuario and u.usuContrasena = :usuContrasena", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "Usuario.findByNombreCedulaApellidos", query = " SELECT u FROM Usuario u WHERE UPPER(u.usuNombre) like :nombre and UPPER(u.usuCedula) like :cedula and UPPER(u.usuPapellido) like :apellido", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
})
public class Usuario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name = "CLI_USUARIO_USU_ID_GENERATOR", sequenceName = "CLINICAUNA.CLI_USUARIO_SEQ01", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CLI_USUARIO_USU_ID_GENERATOR")
    @Basic(optional = false)
    @Column(name = "USU_ID")
    private Long usuId;
    @Basic(optional = false)
    @Column(name = "USU_NOMBRE")
    private String usuNombre;
    @Basic(optional = false)
    @Column(name = "USU_PAPELLIDO")
    private String usuPapellido;
    @Column(name = "USU_SAPELLIDO")
    private String usuSapellido;
    @Basic(optional = false)
    @Column(name = "USU_CORREO")
    private String usuCorreo;
    @Basic(optional = false)
    @Column(name = "USU_TIPO")
    private String usuTipo;
    @Column(name = "USU_IDIOMA")
    private String usuIdioma;
    @Basic(optional = false)
    @Column(name = "USU_CONTRASENA")
    private String usuContrasena;
    @Basic(optional = false)
    @Column(name = "USU_USUARIO")
    private String usuUsuario;
    @Basic(optional = false)
    @Column(name = "USU_ESTADO")
    private String usuEstado;
    @Basic(optional = false)
    @Column(name = "USU_CEDULA")
    private String usuCedula;
    @Version
    @Column(name = "USU_VERSION")
    private Integer usuVersion;
    @OneToMany(mappedBy = "medUsuId")
    private List<Medico> medicoList;

    public Usuario() {
    }

    public Usuario(Long usuId) {
        this.usuId = usuId;
    }

    public Usuario(Long usuId, String usuNombre, String usuPapellido, String usuCorreo, String usuTipo, String usuContrasena, String usuUsuario, String usuEstado, String usuCedula, Integer usuVersion) {
        this.usuId = usuId;
        this.usuNombre = usuNombre;
        this.usuPapellido = usuPapellido;
        this.usuCorreo = usuCorreo;
        this.usuTipo = usuTipo;
        this.usuContrasena = usuContrasena;
        this.usuUsuario = usuUsuario;
        this.usuEstado = usuEstado;
        this.usuCedula = usuCedula;
        this.usuVersion = usuVersion;
    }

    public void actualizar(UsuarioDto usu) {
        this.usuNombre = usu.getUsuNombre();
        this.usuPapellido = usu.getUsuPapellido();
        this.usuSapellido = usu.getUsuSapellido();
        this.usuCedula = usu.getUsuCedula();
        this.usuUsuario = usu.getUsuUsuario();
        this.usuContrasena = usu.getUsuContrasena();
        this.usuCorreo = usu.getUsuCorreo();
        this.usuIdioma = usu.getUsuIdioma();
        this.usuEstado = usu.getUsuEstado();
        this.usuTipo = usu.getUsuTipo();

    }

    public Usuario(UsuarioDto usu) {
        this.usuId = usu.getUsuId();
        actualizar(usu);
    }

    public Long getUsuId() {
        return usuId;
    }

    public void setUsuId(Long usuId) {
        this.usuId = usuId;
    }

    public String getUsuNombre() {
        return usuNombre;
    }

    public void setUsuNombre(String usuNombre) {
        this.usuNombre = usuNombre;
    }

    public String getUsuPapellido() {
        return usuPapellido;
    }

    public void setUsuPapellido(String usuPapellido) {
        this.usuPapellido = usuPapellido;
    }

    public String getUsuSapellido() {
        return usuSapellido;
    }

    public void setUsuSapellido(String usuSapellido) {
        this.usuSapellido = usuSapellido;
    }

    public String getUsuCorreo() {
        return usuCorreo;
    }

    public void setUsuCorreo(String usuCorreo) {
        this.usuCorreo = usuCorreo;
    }

    public String getUsuTipo() {
        return usuTipo;
    }

    public void setUsuTipo(String usuTipo) {
        this.usuTipo = usuTipo;
    }

    public String getUsuIdioma() {
        return usuIdioma;
    }

    public void setUsuIdioma(String usuIdioma) {
        this.usuIdioma = usuIdioma;
    }

    public String getUsuContrasena() {
        return usuContrasena;
    }

    public void setUsuContrasena(String usuContrasena) {
        this.usuContrasena = usuContrasena;
    }

    public String getUsuUsuario() {
        return usuUsuario;
    }

    public void setUsuUsuario(String usuUsuario) {
        this.usuUsuario = usuUsuario;
    }

    public String getUsuEstado() {
        return usuEstado;
    }

    public void setUsuEstado(String usuEstado) {
        this.usuEstado = usuEstado;
    }

    public String getUsuCedula() {
        return usuCedula;
    }

    public void setUsuCedula(String usuCedula) {
        this.usuCedula = usuCedula;
    }

    public Integer getUsuVersion() {
        return usuVersion;
    }

    public void setUsuVersion(Integer usuVersion) {
        this.usuVersion = usuVersion;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (usuId != null ? usuId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Usuario)) {
            return false;
        }
        Usuario other = (Usuario) object;
        if ((this.usuId == null && other.usuId != null) || (this.usuId != null && !this.usuId.equals(other.usuId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Usuario[ usuId=" + usuId + " ]";
    }

    @XmlTransient
    public List<Medico> getMedicoList() {
        return medicoList;
    }

    public void setMedicoList(List<Medico> medicoList) {
        this.medicoList = medicoList;
    }

}

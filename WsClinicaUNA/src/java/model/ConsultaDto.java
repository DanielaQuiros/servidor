/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Dani
 */
@XmlRootElement(name = "ConsultaDto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ConsultaDto 
{
    private Long conId;
    private Integer conFrecuenciacardiaca;
    private Integer conPresion;
    private Long conPeso;
    private Integer conTalla;
    private Integer conTemperatura;
    private Long conImc;
    private String conAnotaciones;
    private String conApuntes;
    private String conPlanatencion;
    private String conObservaciones;
    private String conExamenes;
    private String conTratamiento;
    private CitaDto cita;
    private ExpedienteDto expediente;
    
    public ConsultaDto()
    {
    }
    
    public ConsultaDto(Consulta consulta)
    {
        this.conId = consulta.getConId();
        this.conFrecuenciacardiaca = consulta.getConFrecuenciacardiaca();
        this.conPresion = consulta.getConPresion();
        this.conPeso = consulta.getConPeso();
        this.conTalla = consulta.getConTalla();
        this.conTemperatura = consulta.getConTemperatura();
        this.conImc = consulta.getConImc();
        this.conAnotaciones = consulta.getConAnotaciones();
        this.conApuntes = consulta.getConApuntes();
        this.conPlanatencion = consulta.getConPlanatencion();
        this.conObservaciones = consulta.getConObservaciones();
        this.conExamenes = consulta.getConExamenes();
        this.conTratamiento = consulta.getConTratamiento(); 
        this.cita = new CitaDto(consulta.getConCitId());
        if(consulta.getConExpId()!= null)
            this.expediente = new ExpedienteDto(consulta.getConExpId());
    }

    public Long getConId() {
        return conId;
    }

    public void setConId(Long conId) {
        this.conId = conId;
    }

    public Integer getConFrecCardiaca() {
        return conFrecuenciacardiaca;
    }

    public void setConFrecCardiaca(Integer conFrecuenciacardiaca) {
        this.conFrecuenciacardiaca = conFrecuenciacardiaca;
    }

    public Integer getConPresion() {
        return conPresion;
    }

    public void setConPresion(Integer conPresion) {
        this.conPresion = conPresion;
    }

    public Long getConPeso() {
        return conPeso;
    }

    public void setConPeso(Long conPeso) {
        this.conPeso = conPeso;
    }

    public Integer getConTalla() {
        return conTalla;
    }

    public void setConTalla(Integer conTalla) {
        this.conTalla = conTalla;
    }

    public Integer getConTemperatura() {
        return conTemperatura;
    }

    public void setConTemperatura(Integer conTemperatura) {
        this.conTemperatura = conTemperatura;
    }

    public Long getConImc() {
        return conImc;
    }

    public void setConImc(Long conImc) {
        this.conImc = conImc;
    }

    public String getConAnotaciones() {
        return conAnotaciones;
    }

    public void setConAnotaciones(String conAnotaciones) {
        this.conAnotaciones = conAnotaciones;
    }

    public String getConApuntes() {
        return conApuntes;
    }

    public void setConApuntes(String conApuntes) {
        this.conApuntes = conApuntes;
    }

    public String getConPlanatencion() {
        return conPlanatencion;
    }

    public void setConPlanatencion(String conPlanatencion) {
        this.conPlanatencion = conPlanatencion;
    }

    public String getConObservaciones() {
        return conObservaciones;
    }

    public void setConObservaciones(String conObservaciones) {
        this.conObservaciones = conObservaciones;
    }

    public String getConExamenes() {
        return conExamenes;
    }

    public void setConExamenes(String conExamenes) {
        this.conExamenes = conExamenes;
    }

    public String getConTratamiento() {
        return conTratamiento;
    }

    public void setConTratamiento(String conTratamiento) {
        this.conTratamiento = conTratamiento;
    }

    public CitaDto getCita() {
        return cita;
    }

    public void setCita(CitaDto cita) {
        this.cita = cita;
    }

    public ExpedienteDto getExpediente() {
        return expediente;
    }

    public void setExpediente(ExpedienteDto expediente) {
        this.expediente = expediente;
    }
    
    
}

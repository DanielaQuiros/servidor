/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.List;

/**
 *
 * @author Dani
 */
public class EspecialidadDto 
{
    private Long espId;
    private String espNombre;
    private List<MedicoDto> medicos;
    
    public EspecialidadDto()
    {
    }
    public EspecialidadDto(Especialidad especialidad)
    {
        this.espId = especialidad.getEspId();
        this.espNombre = especialidad.getEspNombre();
    }
    public Long getEspId() {
        return espId;
    }

    public void setEspId(Long espId) {
        this.espId = espId;
    }

    public String getEspNombre() {
        return espNombre;
    }

    public void setEspNombre(String espNombre) {
        this.espNombre = espNombre;
    }

    public List<MedicoDto> getMedicos() {
        return medicos;
    }

    public void setMedicos(List<MedicoDto> medicos) {
        this.medicos = medicos;
    }
    
    
}

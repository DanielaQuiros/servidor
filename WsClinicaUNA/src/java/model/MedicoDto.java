/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import util.LocalTimeAdapter;


/**
 *
 * @author liedu
 */
@XmlRootElement(name = "MedicoDto")
@XmlAccessorType(XmlAccessType.FIELD)
public class MedicoDto {

    private Long medId;
    private String medCodigo;
    private String medFolio;
    private String medCarne;
    @XmlJavaTypeAdapter(LocalTimeAdapter.class)
    private String medIniciojornada;
    @XmlJavaTypeAdapter(LocalTimeAdapter.class)
    private String medFinjornada;
    private String medEspecialidad;
    private Integer medEspaciosPorHora;
    private UsuarioDto medUsuId;
    private List<CitaDto> citaList;
    private EspecialidadDto medEspId;

    public MedicoDto() {
    }

    public MedicoDto(Medico medico) {
        this.medId = medico.getMedId();
        this.medCodigo = medico.getMedCodigo();
        this.medFolio = medico.getMedFolio();
        this.medCarne = medico.getMedCarne();
        this.medIniciojornada = medico.getMedIniciojornada();
        this.medFinjornada = medico.getMedFinjordana();
        this.medEspaciosPorHora = medico.getMedEspaciosporhora();
        this.medUsuId = new UsuarioDto(medico.getMedUsuId());
        if(medico.getMedEspId() != null)
            this.medEspId = new EspecialidadDto(medico.getMedEspId());
    }

    public Long getMedId() {
        return medId;
    }

    public void setMedId(Long medId) {
        this.medId = medId;
    }

    public String getMedCodigo() {
        return medCodigo;
    }

    public void setMedCodigo(String medCodigo) {
        this.medCodigo = medCodigo;
    }

    public String getMedFolio() {
        return medFolio;
    }

    public void setMedFolio(String medFolio) {
        this.medFolio = medFolio;
    }

    public String getMedCarne() {
        return medCarne;
    }

    public void setMedCarne(String medCarne) {
        this.medCarne = medCarne;
    }
    @XmlJavaTypeAdapter(LocalTimeAdapter.class)
    public String getMedIniciojornada() {
        return medIniciojornada;
    }
    @XmlJavaTypeAdapter(LocalTimeAdapter.class)
    public void setMedIniciojornada(String medIniciojornada) {
        this.medIniciojornada = medIniciojornada;
    }
    @XmlJavaTypeAdapter(LocalTimeAdapter.class)
    public String getMedFinjornada() {
        return medFinjornada;
    }
    @XmlJavaTypeAdapter(LocalTimeAdapter.class)
    public void setMedFinjornada(String medFinjordana) {
        this.medFinjornada = medFinjordana;
    }
    public Integer getMedEspaciosPorHora() {
        return medEspaciosPorHora;
    }

    public void setMedEspaciosPorHora(Integer medEspaciosPorHora) {
        this.medEspaciosPorHora = medEspaciosPorHora;
    }
    public UsuarioDto getMedUsuId() {
        return medUsuId;
    }

    public void setMedUsuId(UsuarioDto medUsuId) {
        this.medUsuId = medUsuId;
    }

    public List<CitaDto> getCitaList() {
        return citaList;
    }

    public void setCitaList(List<CitaDto> citaList) {
        this.citaList = citaList;
    }

    public EspecialidadDto getMedEspId() {
        return medEspId;
    }

    public void setMedEspId(EspecialidadDto medEspId) {
        this.medEspId = medEspId;
    }

}

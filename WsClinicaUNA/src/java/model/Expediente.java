/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.QueryHint;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Susana
 */
@Entity
@Table(name = "CLI_EXPEDIENTE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Expediente.findAll", query = "SELECT e FROM Expediente e")
    , @NamedQuery(name = "Expediente.findByExpId", query = "SELECT e FROM Expediente e WHERE e.expId = :expId")
    , @NamedQuery(name = "Expediente.findByExpAntecedentespatologicos", query = "SELECT e FROM Expediente e WHERE e.expAntecedentespatologicos = :expAntecedentespatologicos")
    , @NamedQuery(name = "Expediente.findByExpHospitalizaciones", query = "SELECT e FROM Expediente e WHERE e.expHospitalizaciones = :expHospitalizaciones")
    , @NamedQuery(name = "Expediente.findByExpOperaciones", query = "SELECT e FROM Expediente e WHERE e.expOperaciones = :expOperaciones")
    , @NamedQuery(name = "Expediente.findByExpAlergias", query = "SELECT e FROM Expediente e WHERE e.expAlergias = :expAlergias")
    , @NamedQuery(name = "Expediente.findByExpTratamientos", query = "SELECT e FROM Expediente e WHERE e.expTratamientos = :expTratamientos")
    , @NamedQuery(name = "Expediente.findByExpAntfamiliar", query = "SELECT e FROM Expediente e WHERE e.expAntfamiliar = :expAntfamiliar")
    , @NamedQuery(name = "Expediente.findByExpVersion", query = "SELECT e FROM Expediente e WHERE e.expVersion = :expVersion")
    , @NamedQuery(name = "Expediente.findByPacId", query = "SELECT e FROM Expediente e WHERE e.pacienteList = :pacienteList", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
})
public class Expediente implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "CLI_EXPEDIENTE_EXP_ID_GENERATOR", sequenceName = "CLINICAUNA.CLI_EXPEDIENTE_SEQ06", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CLI_EXPEDIENTE_EXP_ID_GENERATOR")
    @Basic(optional = false)
    @Column(name = "EXP_ID")
    private Long expId;
    @Column(name = "EXP_ANTECEDENTESPATOLOGICOS")
    private String expAntecedentespatologicos;
    @Column(name = "EXP_HOSPITALIZACIONES")
    private String expHospitalizaciones;
    @Column(name = "EXP_OPERACIONES")
    private String expOperaciones;
    @Column(name = "EXP_ALERGIAS")
    private String expAlergias;
    @Column(name = "EXP_TRATAMIENTOS")
    private String expTratamientos;
    @Column(name = "EXP_ANTFAMILIAR")
    private String expAntfamiliar;
    @Version
    @Column(name = "EXP_VERSION")
    private Long expVersion;
    @OneToMany(mappedBy = "arcExpId")
    private List<Archivos> archivosList;
    @OneToMany(mappedBy = "pacExpId")
    private List<Paciente> pacienteList;
    @OneToMany(mappedBy = "conExpId")
    private List<Consulta> consultaList;

    public Expediente() {
    }

    public Expediente(ExpedienteDto exp) {
        this.expId = exp.getId();
        actualizar(exp);
    }

    public void actualizar(ExpedienteDto exp) {
        this.expAlergias = exp.getAlergias();
        this.expAntecedentespatologicos = exp.getAntePalogicos();
        this.expAntfamiliar = exp.getAnteFamiliares();
        this.expHospitalizaciones = exp.getHospitalizaciones();
        this.expOperaciones = exp.getOperaciones();
        this.expTratamientos = exp.getTratamientos();
    }

    public Expediente(Long expId) {
        this.expId = expId;
    }

    public Expediente(Long expId, Long expVersion) {
        this.expId = expId;
        this.expVersion = expVersion;
    }

    public Long getExpId() {
        return expId;
    }

    public void setExpId(Long expId) {
        this.expId = expId;
    }

    public String getExpAntecedentespatologicos() {
        return expAntecedentespatologicos;
    }

    public void setExpAntecedentespatologicos(String expAntecedentespatologicos) {
        this.expAntecedentespatologicos = expAntecedentespatologicos;
    }

    public String getExpHospitalizaciones() {
        return expHospitalizaciones;
    }

    public void setExpHospitalizaciones(String expHospitalizaciones) {
        this.expHospitalizaciones = expHospitalizaciones;
    }

    public String getExpOperaciones() {
        return expOperaciones;
    }

    public void setExpOperaciones(String expOperaciones) {
        this.expOperaciones = expOperaciones;
    }

    public String getExpAlergias() {
        return expAlergias;
    }

    public void setExpAlergias(String expAlergias) {
        this.expAlergias = expAlergias;
    }

    public String getExpTratamientos() {
        return expTratamientos;
    }

    public void setExpTratamientos(String expTratamientos) {
        this.expTratamientos = expTratamientos;
    }

    public String getExpAntfamiliar() {
        return expAntfamiliar;
    }

    public void setExpAntfamiliar(String expAntfamiliar) {
        this.expAntfamiliar = expAntfamiliar;
    }

    public Long getExpVersion() {
        return expVersion;
    }

    public void setExpVersion(Long expVersion) {
        this.expVersion = expVersion;
    }

    @XmlTransient
    public List<Archivos> getArchivosList() {
        return archivosList;
    }

    public void setArchivosList(List<Archivos> archivosList) {
        this.archivosList = archivosList;
    }

    @XmlTransient
    public List<Paciente> getPacienteList() {
        return pacienteList;
    }

    public void setPacienteList(List<Paciente> pacienteList) {
        this.pacienteList = pacienteList;
    }

    @XmlTransient
    public List<Consulta> getConsultaList() {
        return consultaList;
    }

    public void setConsultaList(List<Consulta> consultaList) {
        this.consultaList = consultaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (expId != null ? expId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Expediente)) {
            return false;
        }
        Expediente other = (Expediente) object;
        if ((this.expId == null && other.expId != null) || (this.expId != null && !this.expId.equals(other.expId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Expediente[ expId=" + expId + " ]";
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.time.LocalDate;
import java.time.ZoneId;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import util.LocalDateAdapter;

/**
 *
 * @author Susana
 */
@XmlRootElement(name = "ArchivoDto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ArchivoDto {
    private Long id;
    private String ruta;
    private String tipo;
    private String anotaciones;
    private String nombreExamen;
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    private LocalDate fecha; 
    private ExpedienteDto expediente; 
    private String archivo;

    public ArchivoDto(Long id, String ruta, String tipo, String anoaciones, String nombreExamen, LocalDate fecha, ExpedienteDto expediente) {
        this.id = id;
        this.ruta = ruta;
        this.tipo = tipo;
        this.anotaciones = anoaciones;
        this.nombreExamen = nombreExamen;
        this.fecha = fecha;
        this.expediente = expediente;
    }
    
    public ArchivoDto(){}
    
    public ArchivoDto(Archivos archivo) {
        this.id = archivo.getArcId();
        this.ruta = archivo.getArcRuta();
        this.tipo = archivo.getArcTipo();
        this.anotaciones = archivo.getArcAnotaciones();
        this.nombreExamen = archivo.getArcNombreexamen();
        if(archivo.getArcFecha() != null)
            this.fecha = archivo.getArcFecha().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        this.expediente = new ExpedienteDto(archivo.getArcExpId());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getNombreExamen() {
        return nombreExamen;
    }

    public void setNombreExamen(String nombreExamen) {
        this.nombreExamen = nombreExamen;
    }

    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    public LocalDate getFecha() {
        return fecha;
    }

    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public ExpedienteDto getExpediente() {
        return expediente;
    }

    public void setExpediente(ExpedienteDto expediente) {
        this.expediente = expediente;
    }

    public String getAnotaciones() {
        return anotaciones;
    }

    public void setAnotaciones(String anotaciones) {
        this.anotaciones = anotaciones;
    }

    public String getArchivo() {
        return archivo;
    }

    public void setArchivo(String archivo) {
        this.archivo = archivo;
    }
    
    
    
}

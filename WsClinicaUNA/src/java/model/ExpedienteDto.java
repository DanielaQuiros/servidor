/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Susana
 */
@XmlRootElement(name = "ExpedienteDto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ExpedienteDto {
    private Long id;
    private String anteFamiliares;
    private String antePalogicos;
    private String alergias;
    private String hospitalizaciones;
    private String tratamientos; 
    private String operaciones;
    private Long version;
    private PacienteDto paciente;
    
    public ExpedienteDto(){
    
    }

    public ExpedienteDto(Long id, String anteFamiliares, String antePalogicos, String alergias, String hospitalizaciones, String tratamientos, String operaciones, Long version) {
        this.id = id;
        this.anteFamiliares = anteFamiliares;
        this.antePalogicos = antePalogicos;
        this.alergias = alergias;
        this.hospitalizaciones = hospitalizaciones;
        this.tratamientos = tratamientos;
        this.operaciones = operaciones;
        this.version = version;
    }
    
    public ExpedienteDto(Expediente exp) {
        this.id = exp.getExpId();
        this.anteFamiliares = exp.getExpAntfamiliar();
        this.antePalogicos = exp.getExpAntecedentespatologicos();
        this.alergias = exp.getExpAlergias();
        this.hospitalizaciones = exp.getExpHospitalizaciones();
        this.tratamientos = exp.getExpTratamientos();
        this.operaciones = exp.getExpOperaciones();
        this.version = exp.getExpVersion();
        if(exp.getPacienteList()!= null && exp.getPacienteList().get(0) != null){
            this.paciente = new PacienteDto(exp.getPacienteList().get(0));
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAnteFamiliares() {
        return anteFamiliares;
    }

    public void setAnteFamiliares(String anteFamiliares) {
        this.anteFamiliares = anteFamiliares;
    }

    public String getAntePalogicos() {
        return antePalogicos;
    }

    public void setAntePalogicos(String antePalogicos) {
        this.antePalogicos = antePalogicos;
    }

    public String getAlergias() {
        return alergias;
    }

    public void setAlergias(String alergias) {
        this.alergias = alergias;
    }

    public String getHospitalizaciones() {
        return hospitalizaciones;
    }

    public void setHospitalizaciones(String hospitalizaciones) {
        this.hospitalizaciones = hospitalizaciones;
    }

    public String getTratamientos() {
        return tratamientos;
    }

    public void setTratamientos(String tratamientos) {
        this.tratamientos = tratamientos;
    }

    public String getOperaciones() {
        return operaciones;
    }

    public void setOperaciones(String operaciones) {
        this.operaciones = operaciones;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public PacienteDto getPaciente() {
        return paciente;
    }

    public void setPaciente(PacienteDto paciente) {
        this.paciente = paciente;
    }
    
    
}

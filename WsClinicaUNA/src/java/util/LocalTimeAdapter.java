/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 *
 * @author Dani
 */
public class LocalTimeAdapter extends XmlAdapter<String, LocalTime>
{

    @Override
    public LocalTime unmarshal(String time) throws Exception 
    {
       try 
       {
            if (time == null) 
            {
                return null;
            } 
            else 
            {
                return LocalTime.parse(time, DateTimeFormatter.ISO_LOCAL_TIME);
            }
        } 
       catch (Exception ex) 
       {
           System.out.println(ex.toString());
           throw ex;
       }
    }

    @Override
    public String marshal(LocalTime time) throws Exception 
    {
        try 
        {
            if (time == null) 
            {
                return null;
            } 
            else 
            {
                return time.format(DateTimeFormatter.ISO_LOCAL_TIME);
            }
        } 
        catch (Exception ex) 
        {
            System.out.println(ex.toString());
            throw ex;
        }
    }
    
}

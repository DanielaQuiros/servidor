/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import model.CitaDto;
import model.PacienteDto;
import service.CitaService;
import service.PacienteService;
import util.CodigoRespuesta;
import util.Respuesta;

/**
 *
 * @author liedu
 */
@Path("/CitaController")
public class CitaController {

    @EJB
    CitaService citaService;

    @POST
    @Path("/guardarCita")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response guardarCita(CitaDto citaDto) {
        try {
            Respuesta respuesta = citaService.guardarCita(citaDto);
            if (!respuesta.getEstado()) {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }
            return Response.ok((CitaDto) respuesta.getResultado("Cita")).build();
        } catch (Exception ex) {
            Logger.getLogger(PacienteController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error guardando el paciente").build();
        }
    }

    @GET
    @Path("/allCitas/{MedId}/{CitFecha}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response allCitas(@PathParam("MedId") Long MedId, @PathParam("CitFecha") String CitFecha) {
        try {
            Respuesta res = citaService.allCitas(MedId, CitFecha);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            List<CitaDto> citasDto = new ArrayList<>();
            citasDto = (List<CitaDto>) res.getResultado("Citas");
            return Response.ok(new GenericEntity<List<CitaDto>>(citasDto) {
            }).build();
        } catch (Exception ex) {
            Logger.getLogger(CitaController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo las citas").build();
        }
    }

    @DELETE
    @Path("/eliminarCita/{citId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response eliminarCita(@PathParam("citId") Long citId) {
        try {
            Respuesta respuesta = citaService.eliminarCita(citId);
            if (!respuesta.getEstado()) {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }
            return Response.ok().build();
        } catch (Exception ex) {
            Logger.getLogger(MedicoController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo el medico").build();
        }
    }

    @GET
    @Path("/getCitas/{MedName}/{CitFecha}/{PaciName}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCitas(@PathParam("MedName") String MedName, @PathParam("CitFecha") String CitFecha, @PathParam("PaciName") String PaciName) {
        try {
            Respuesta res = citaService.getCitas(MedName, CitFecha, PaciName);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            List<CitaDto> citasDto = new ArrayList<>();
            citasDto = (List<CitaDto>) res.getResultado("Citas");
            return Response.ok(new GenericEntity<List<CitaDto>>(citasDto) {
            }).build();
        } catch (Exception ex) {
            Logger.getLogger(CitaController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo las citas").build();
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import model.ArchivoDto;
import model.ExpedienteDto;
import service.ArchivoService;
import util.CodigoRespuesta;
import util.Respuesta;

@Path("/ArchivoController")
public class ArchivoController {
    @EJB 
    ArchivoService archivoService;
    
    @POST
    @Path("/guardarArchivo") 
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response guardarArchivos(ArchivoDto archivoDto) {
        try {
            Respuesta respuesta = archivoService.guardarArchivos(archivoDto);
            if (!respuesta.getEstado()) {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }
            return Response.ok((ArchivoDto) respuesta.getResultado("Archivos")).build();
        } catch (Exception ex) {
            Logger.getLogger(ArchivoController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error guardando el archivo").build();
        }
    }

    @GET
    @Path("/Archivos/{idExp}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getArchivos(@PathParam("idExp") Long expId) {
        try {
            Respuesta res = archivoService.getArchivos(expId);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            List<ArchivoDto> archivosDto = new ArrayList<>();
            archivosDto = (List<ArchivoDto>) res.getResultado("Archivos");
            return Response.ok(new GenericEntity<List<ArchivoDto>> (archivosDto) {}).build();
        } catch (Exception ex) {
            Logger.getLogger(ArchivoController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo los archivos").build();
        }
    }
    
    @GET
    @Path("/Archivo/{archId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getArchivo(@PathParam("archId") Long archId) {
        try {
            Respuesta respuesta = archivoService.getArchivo(archId);
            if (!respuesta.getEstado()) {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }

            return Response.ok((ArchivoDto) respuesta.getResultado("Archivo")).build();
        } catch (Exception ex) {
            Logger.getLogger(ArchivoController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo los archivos").build();
        }
    }
    
    @DELETE
    @Path("/eliminarArchivo/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response eliminarArchivos(@PathParam("id") Long id) {
        try {
            Respuesta res = archivoService.eliminarArchivos(id);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok().build();
        } catch (Exception ex) {
            Logger.getLogger(ArchivoController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error eliminando el archivos").build();
        }
    }
    
}

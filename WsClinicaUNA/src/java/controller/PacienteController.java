
package controller;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import model.PacienteDto;
import service.PacienteService;
import util.CodigoRespuesta;
import util.Respuesta;


@Path("/PacienteController")
public class PacienteController {
    
    @EJB
    PacienteService pacienteService;
    
    @POST
    @Path("/guardarPaciente") 
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response guardarPaciente(PacienteDto pacienteDto) {
        try {
            Respuesta respuesta = pacienteService.guardarPaciente(pacienteDto);
            if (!respuesta.getEstado()) {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }
            return Response.ok((PacienteDto) respuesta.getResultado("Paciente")).build();
        } catch (Exception ex) {
            Logger.getLogger(PacienteController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error guardando el paciente").build();
        }
    }

    @GET
    @Path("/pacientes/{cedula}/{nombre}/{apellidos}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPacientes(@PathParam("cedula") String cedula, @PathParam("nombre") String nombre, @PathParam("apellidos") String apellidos) {
        try {
            Respuesta res = pacienteService.getPacientes(cedula, nombre, apellidos);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            List<PacienteDto> pacientesDto = new ArrayList<>();
            pacientesDto = (List<PacienteDto>) res.getResultado("Pacientes");
            return Response.ok(new GenericEntity<List<PacienteDto>> (pacientesDto) {}).build();
        } catch (Exception ex) {
            Logger.getLogger(PacienteController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo los pacientes").build();
        }
    }
    
    @DELETE
    @Path("/eliminarPaciente/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response eliminarPaciente(@PathParam("id") Long id) {
        try {
            Respuesta res = pacienteService.eliminarPaciente(id);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok().build();
        } catch (Exception ex) {
            Logger.getLogger(PacienteController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error eliminando el pacientes").build();
        }
    }
}

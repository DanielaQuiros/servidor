/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import model.EspecialidadDto;
import service.EspecialidadService;
import util.CodigoRespuesta;
import util.Respuesta;

/**
 *
 * @author Dani
 */
@Path("/EspecialidadController")
public class EspecialidadController 
{
    @EJB
    EspecialidadService espService;
    
    @POST
    @Path("/guardarEspecialidad") 
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response guardarEspecialidad(EspecialidadDto usuarioDto) 
    {
        try 
        {
            Respuesta respuesta = espService.guardarEspecialidad(usuarioDto);
            if (!respuesta.getEstado()) 
            {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }
            return Response.ok((EspecialidadDto) respuesta.getResultado("Especialidad")).build();
        } 
        catch (Exception ex) 
        {
            Logger.getLogger(EspecialidadController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error guardando la especialidad").build();
        }
    }
    @GET
    @Path("/getEspecialidad/{nombre}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getEspecialidad(@PathParam("nombre") String nombre) 
    {
        try 
        {
            Respuesta res = espService.getEspecialidad(nombre);
            if (!res.getEstado()) 
            {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
           List<EspecialidadDto> especialidadDto = new ArrayList();
           especialidadDto = (List<EspecialidadDto>) res.getResultado("Especialidad");
           return Response.ok(new GenericEntity<List<EspecialidadDto>>(especialidadDto){}).build();
        } 
        catch (Exception ex) 
        {
            Logger.getLogger(EspecialidadController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo la especialidad").build();
        }
    }
    @GET
    @Path("/getEspecialidades")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getEspecialidades() 
    {
        try 
        {
            Respuesta res = espService.getEspecialidades();
            if (!res.getEstado()) 
            {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            List<EspecialidadDto> especialidadDto = new ArrayList();
            especialidadDto = (List<EspecialidadDto>) res.getResultado("Especialidades");
            return Response.ok(new GenericEntity<List<EspecialidadDto>>(especialidadDto){}).build();
        } 
        catch (Exception ex) 
        {
            Logger.getLogger(EspecialidadController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo la especialidad").build();
        }
    }
    @DELETE
    @Path("/eliminarEspecialidad/{espId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response eliminarEspecialidad(@PathParam("espId") Long espId)
    {
        try
        {
            Respuesta respuesta=espService.eliminarEspecialidad(espId);
            if(!respuesta.getEstado())
            {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }
            return Response.ok().build();
        }
        catch(Exception ex)
        {
            Logger.getLogger(EspecialidadController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error eliminando la especialidad").build();
        }
    }
     @GET
    @Path("/getEsp")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getEsp() 
    {
        try 
        {
            Respuesta res = espService.getEsp();
            if (!res.getEstado()) 
            {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
           List<EspecialidadDto> especialidadDto = new ArrayList();
           especialidadDto = (List<EspecialidadDto>) res.getResultado("Especialidad");
           return Response.ok(new GenericEntity<List<EspecialidadDto>>(especialidadDto){}).build();
        } 
        catch (Exception ex) 
        {
            Logger.getLogger(EspecialidadController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo la especialidad").build();
        }
    }
}
 

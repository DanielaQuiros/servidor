/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import model.PacienteDto;
import net.sf.jasperreports.engine.JasperPrint;
import reportes.GenerarReporte;
import util.CodigoRespuesta;
import util.Respuesta;

/**
 *
 * @author liedu
 */
@Path("/PacienteController")
public class ReporteController {

    @EJB
    GenerarReporte report;

    @GET
    @Path("/agenda/{IdMedico}/{inicio}/{fin}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getRerporMedico(@PathParam("IdMedico") Long idMed, @PathParam("inicio") String inicio, @PathParam("fin") String fin) {
        try {
            Respuesta res = report.reporteMed(idMed, inicio, fin);
            return Response.ok((String) res.getResultado("Reporte")).build();
        } catch (Exception ex) {
            Logger.getLogger(ReporteController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error al generar el reporte").build();

        }
    }
    @GET
    @Path("/especialidad/{IdEspe}/{inicio}/{fin}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getReportEspe(@PathParam("IdEspe") String IdEspe, @PathParam("inicio") String inicio, @PathParam("fin") String fin) {
        try {
            Respuesta res = report.reporEspecilaidad(IdEspe, inicio, fin);
            return Response.ok((String) res.getResultado("Reporte")).build();
        } catch (Exception ex) {
            Logger.getLogger(ReporteController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error al generar el reporte").build();

        }
    }

    @GET
    @Path("/paciente/{idPaciente}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getRerporPaci(@PathParam("idPaciente") Long idPaciente) {
        try {
            Respuesta res = report.reporPaciente(idPaciente);
            return Response.ok((String) res.getResultado("Reporte")).build();
        } catch (Exception ex) {
            Logger.getLogger(ReporteController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error al generar el reporte").build();
        }
    }
    
    @GET
    @Path("/Prueba")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getRerporPrueba() {
        try {
            Respuesta res = report.reporte();
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok((JasperPrint) res.getResultado("Reporte")).build();

        } catch (Exception ex) {
            Logger.getLogger(ReporteController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error al generar el reporte").build();
        }
    }

}

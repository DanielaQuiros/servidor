/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import model.ExpedienteDto;
import service.ExpedienteService;
import util.CodigoRespuesta;
import util.Respuesta;

@Path("/ExpedienteController")
public class ExpedienteController {
    @EJB
    ExpedienteService expedienteService;
    
    @POST
    @Path("/guardarExpediente") 
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response guardarExpediente(ExpedienteDto expedienteDto) {
        try {
            Respuesta respuesta = expedienteService.guardarExpediente(expedienteDto);
            if (!respuesta.getEstado()) {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }
            return Response.ok((ExpedienteDto) respuesta.getResultado("Expediente")).build();
        } catch (Exception ex) {
            Logger.getLogger(ExpedienteController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error guardando el expediente").build();
        }
    }

    @GET
    @Path("/expediente/{pacId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getExpedientes(@PathParam("pacId") Long pacId) {
        try {
            Respuesta res = expedienteService.getExpedientes(pacId);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            List<ExpedienteDto> expedientesDto = new ArrayList<>();
            return Response.ok((ExpedienteDto) res.getResultado("expediente")).build();
        } catch (Exception ex) {
            Logger.getLogger(ExpedienteController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo los expedientes").build();
        }
    }
    
    @DELETE
    @Path("/eliminarExpediente/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response eliminarExpediente(@PathParam("id") Long id) {
        try {
            Respuesta res = expedienteService.eliminarExpediente(id);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok().build();
        } catch (Exception ex) {
            Logger.getLogger(ExpedienteController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error eliminando el expedientes").build();
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import model.ConsultaDto;
import service.ConsultaService;
import util.CodigoRespuesta;
import util.Respuesta;

/**
 *
 * @author Dani
 */
@Path("/ConsultaController")
public class ConsultaController 
{
    @EJB
    ConsultaService consultaService;
    
    @POST
    @Path("/guardarConsulta") 
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response guardarConsulta(ConsultaDto consultaDto)
    {
        try
        {
            Respuesta respuesta = consultaService.guardarConsulta(consultaDto);
            if (!respuesta.getEstado()) 
            {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }
            return Response.ok((ConsultaDto) respuesta.getResultado("Consulta")).build();
        }
        catch(Exception ex)
        {
            Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error guardando la consulta").build();
        }
    }
    
    @GET
    @Path("/getConsulta/{citId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getConsulta(@PathParam("citId")Long citId)
    {
        try
        {
            Respuesta respuesta = consultaService.getConsulta(citId);
            if(!respuesta.getEstado())
            {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }
            ConsultaDto consulta = (ConsultaDto) respuesta.getResultado("Consultas");
            return Response.ok(consulta).build();
        }
        catch(Exception ex)
        {
            Logger.getLogger(ConsultaController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error al obtener la consulta").build();
        }
    }
    
    @GET
    @Path("/getConsultas/{pacId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getConsultas(@PathParam("pacId")Long pacId)
    {
        try
        {
            Respuesta respuesta = consultaService.getConsultas(pacId);
            if(!respuesta.getEstado())
            {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }
            List<ConsultaDto> consultaDto = new ArrayList();
            consultaDto = (List<ConsultaDto>) respuesta.getResultado("Consultas");
            return Response.ok(new GenericEntity<List<ConsultaDto>>(consultaDto){}).build();
        }
        catch(Exception ex)
        {
            Logger.getLogger(ConsultaController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error al obtener la consulta").build();
        }
    }
    
    @DELETE
    @Path("/eliminarConsulta/{conId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response eliminarConsulta(@PathParam("conId")Long conId)
    {
        try
        {
            Respuesta respuesta = consultaService.eliminarConsulta(conId);
            if(!respuesta.getEstado())
            {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }
            return Response.ok().build();
        }
        catch(Exception ex)
        {
           Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error al eliminar la consulta").build();  
        }
    }
    
}

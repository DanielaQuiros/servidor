/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import model.UsuarioDto;
import service.UsuarioService;
import util.CodigoRespuesta;
import util.Respuesta;

/**
 *
 * @author Dani
 */
@Path("/UsuarioController")
public class UsuarioController 
{
    @EJB
    UsuarioService usuarioService;
    
    @GET
    @Path("/getUsuario/{usuario}/{contra}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getUsuario(@PathParam("usuario") String usuario, @PathParam("contra") String contra) 
    {
        try 
        {
            Respuesta res = usuarioService.getUsuario(usuario,contra);
            if (!res.getEstado()) 
            {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            UsuarioDto usu = (UsuarioDto) res.getResultado("Usuario");
            return Response.ok((UsuarioDto) res.getResultado("Usuario")).build();
        } 
        catch (Exception ex) 
        {
            Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo el usuario").build();
        }
    }
    @POST
    @Path("/guardarUsuario") 
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response guardarUsuario(UsuarioDto usuarioDto) 
    {
        try 
        {
            Respuesta respuesta = usuarioService.guardarUsuario(usuarioDto);
            if (!respuesta.getEstado()) 
            {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }
            return Response.ok((UsuarioDto) respuesta.getResultado("Usuario")).build();
        } 
        catch (Exception ex) 
        {
            Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error guardando el usuario").build();
        }
    }
    @GET
    @Path("/recuperarUsuario/{correo}/{contra}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response recuperarUsuario(@PathParam("correo")String correo,@PathParam("contra")String contra)
    {
        try
        {
            Respuesta respuesta=usuarioService.recuperarUsuario(correo, contra);
            if(!respuesta.getEstado())
            {
                return Response.ok((UsuarioDto) respuesta.getResultado("Usuario")).build();
            }
            return Response.ok((UsuarioDto) respuesta.getResultado("Usuario")).build();
        }
        catch(Exception ex)
        {
            Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error recuperando el usuario").build();
        }
    }
    @GET
    @Path("/activarUsuario/{usuId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response activarUsuario(@PathParam("usuId")Long usuId)
    {
        try
        {
            Respuesta respuesta=usuarioService.activarUsuario(usuId);
            if(!respuesta.getEstado())
            {
                return Response.ok((UsuarioDto) respuesta.getResultado("Usuario")).build();
            }
            UsuarioDto usuario = (UsuarioDto) respuesta.getResultado("Usuario");
            System.out.println(usuario.getUsuEstado());
            return Response.ok((UsuarioDto) respuesta.getResultado("Usuario")).build();
        }
        catch(Exception ex)
        {
            Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error recuperando el usuario").build();
        }
    }
    @DELETE
    @Path("/eliminarUsuario/{usuId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response eliminarUsuario(@PathParam("usuId") Long usuId)
    {
        try
        {
            Respuesta respuesta=usuarioService.eliminarUsuario(usuId);
            if(!respuesta.getEstado())
            {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }
            return Response.ok().build();
        }
        catch(Exception ex)
        {
            Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error eliminando el usuario").build();
        }
    }
    @GET
    @Path("/getUsuarios/{nombre}/{cedula}/{apellido}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getUsuarios(@PathParam("nombre")String nombre,@PathParam("cedula") String cedula, @PathParam("apellido") String apellido)
    {
        try
        {
            Respuesta respuesta = usuarioService.getUsuarios(nombre.trim(), cedula.trim(), apellido.trim());
            if(!respuesta.getEstado())
            {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }
            List<UsuarioDto> usuariosDto = new ArrayList();
            usuariosDto = (List<UsuarioDto>) respuesta.getResultado("Usuarios");
            return Response.ok(new GenericEntity<List<UsuarioDto>>(usuariosDto){}).build();
        }
        catch(Exception ex)
        {
            Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo los usuarios").build();
        }
    }
    @GET
    @Path("/validarUsuario/{usuUsuario}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response validarUsuario(@PathParam("usuUsuario")String usuario)
    {
        try
        {
            Respuesta respuesta=usuarioService.validarUsuario(usuario);
            if(!respuesta.getEstado())
            {
                return Response.ok((UsuarioDto) respuesta.getResultado("Usuario")).build();
            }
            UsuarioDto usu = (UsuarioDto) respuesta.getResultado("Usuario");
            return Response.ok((UsuarioDto) respuesta.getResultado("Usuario")).build();
        }
        catch(Exception ex)
        {
            Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo los usuarios").build();
        }
    }
    @GET
    @Path("/getUsuId/{usuario}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getUsuId(@PathParam("usuario")Long usuario)
    {
        try
        {
            Respuesta respuesta=usuarioService.getUsuId(usuario);
            if(!respuesta.getEstado())
            {
                return Response.ok((UsuarioDto) respuesta.getResultado("Usuario")).build();
            }
            UsuarioDto usu = (UsuarioDto) respuesta.getResultado("Usuario");
            return Response.ok((UsuarioDto) respuesta.getResultado("Usuario")).build();
        }
        catch(Exception ex)
        {
            Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo los usuarios").build();
        }
    }
}

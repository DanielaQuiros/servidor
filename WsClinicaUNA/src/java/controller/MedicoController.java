/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import model.MedicoDto;
import service.MedicoService;
import util.CodigoRespuesta;
import util.Respuesta;

/**
 *
 * @author Dani
 */
@Path("/MedicoController")
public class MedicoController 
{
    @EJB
    MedicoService medicoService;
    
    @POST
    @Path("/guardarMedico") 
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response guardarMedico(MedicoDto medicoDto) 
    {
        try 
        {
            Respuesta respuesta = medicoService.guardarMedico(medicoDto);
            if (!respuesta.getEstado()) 
            {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }
            return Response.ok((MedicoDto) respuesta.getResultado("Medico")).build();
        } 
        catch (Exception ex) 
        {
            Logger.getLogger(MedicoController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error guardando el medico").build();
        }
    }
    
    @DELETE
    @Path("/eliminarMedico/{medId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response eliminarMedico(@PathParam("medId")Long medId)
    {
        try
        {
            Respuesta respuesta = medicoService.eliminarMedico(medId);
            if (!respuesta.getEstado()) 
            {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }
            return Response.ok((MedicoDto) respuesta.getResultado("Medico")).build();
        } 
        catch (Exception ex) 
        {
            Logger.getLogger(MedicoController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo el medico").build();
        }
    }
    @GET
    @Path("/getMedico/{folio}/{carne}/{codigo}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getMedico(@PathParam("folio")String folio,@PathParam("carne") String carne, @PathParam("codigo") String codigo)
    {
        try
        {
            Respuesta respuesta = medicoService.getMedico(folio, carne, codigo); 
            if(!respuesta.getEstado())
            {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }
            List<MedicoDto> medicosDto = new ArrayList();
            medicosDto = (List<MedicoDto>) respuesta.getResultado("Medicos");
            return Response.ok(new GenericEntity<List<MedicoDto>>(medicosDto){}).build();
        }
        catch(Exception ex)
        {
            Logger.getLogger(MedicoController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo los medicos").build();
        }
    }
    @GET
    @Path("/getEspecialidad/{EspId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getEspecialidad(@PathParam("EspId")Long EspId)
    {
        try 
        {
            Respuesta respuesta = medicoService.getEspecialidad(EspId); 
            if(!respuesta.getEstado())
            {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }
            List<MedicoDto> medicosDto = new ArrayList();
            medicosDto = (List<MedicoDto>) respuesta.getResultado("EspecialidadM");
            return Response.ok(new GenericEntity<List<MedicoDto>>(medicosDto){}).build();
        } 
        catch (Exception ex) 
        {
            Logger.getLogger(MedicoController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo las especialidades").build();
        }
    }
    @GET
    @Path("/getMedico/{usu}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getMedico(@PathParam("usu")Long usu)
    {
        try 
        {
            Respuesta respuesta = medicoService.getMedico(usu); 
            if(!respuesta.getEstado())
            {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }
            return Response.ok((MedicoDto) respuesta.getResultado("Medico")).build();
        } 
        catch (Exception ex) 
        {
            Logger.getLogger(MedicoController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo las especialidades").build();
        }
    }
}

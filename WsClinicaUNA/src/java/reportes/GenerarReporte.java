/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reportes;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PreDestroy;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import model.Especialidad;
import model.Medico;
import model.Paciente;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import util.CodigoRespuesta;
import util.Respuesta;

/**
 *
 * @author liedu
 */
@Stateless
@LocalBean
public class GenerarReporte {

    private static final Logger LOG = Logger.getLogger(GenerarReporte.class.getName());
    @PersistenceContext(unitName = "WsClinicaUNAPU")
    private EntityManager em;

    public GenerarReporte() {
    }

    public Respuesta reporte() {
        try {
            String cadenaConexion = "jdbc:oracle:thin:@localhost:1521:XE";
            Class.forName("oracle.jdbc.OracleDriver");
            Connection con = DriverManager.getConnection(cadenaConexion,
                    "clinicauna", "una");
            URL in = this.getClass().getResource("prueba.jasper");
            String dir = dirArchivo();
            JasperReport report = (JasperReport) JRLoader.loadObject(in);
            Map parametros = new HashMap();
            parametros.put("Estado", "P");
            parametros.put("Doctor", "%%");
            JasperPrint print = JasperFillManager.fillReport(report, parametros, con);
            JasperExportManager.exportReportToPdfFile(print, dir + "Prueba.pdf");
            JasperExportManager.exportReportToPdf(print);
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Reporte", "Correcto");

        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio con reporte", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al generar el reporte prueba.", "reporteMed " + ex.getMessage());

        }
    }

    public static String dirArchivo() {
        String dre = GenerarReporte.class.getSimpleName() + ".class";
        String rutaca = GenerarReporte.class.getResource(dre).toString();
        rutaca = rutaca.replace("/", "\\\\");
        rutaca = rutaca.replace("file:", "");
        rutaca = rutaca.replace("\\\\C", "C");
        rutaca = rutaca.replace("\\\\build", "");
        rutaca = rutaca.replace("\\\\web", "");
        rutaca = rutaca.replace("\\\\WEB-INF", "");
        rutaca = rutaca.replace("\\\\classes", "");
        rutaca = rutaca.replace("\\\\reportes", "");
        rutaca = rutaca.replace("\\\\GenerarReporte.class", "");
        rutaca = rutaca.replace("\\\\WsClinicaUNA", "\\\\");
        rutaca = rutaca.replace("%20", "");
        rutaca = rutaca + "WsClinicaUNA\\src\\java\\reportes\\";
        //System.out.println("ruta: "+rutaca);
        return rutaca;
    }

    public Respuesta reporteMed(Long idMed, String ini, String end) {
        try {
            Medico med = em.find(Medico.class, idMed);
            Query queryC = em.createNamedQuery("Medico.findCanceladas", Medico.class);
            queryC.setParameter("medId", med.getMedId());
            Long canceladas = (Long) queryC.getSingleResult();

            Query queryA = em.createNamedQuery("Medico.findAtendidas", Medico.class);
            queryA.setParameter("medId", med.getMedId());
            Long atendidas = (Long) queryA.getSingleResult();

            Query queryN = em.createNamedQuery("Medico.findAusentes", Medico.class);
            queryN.setParameter("medId", med.getMedId());
            Long ausentes = (Long) queryN.getSingleResult();

            Query queryP = em.createNamedQuery("Medico.findProgramadas", Medico.class);
            queryP.setParameter("medId", med.getMedId());
            Long programadas = (Long) queryP.getSingleResult();

            Date inicio = Date.from(LocalDate.parse(ini).atStartOfDay(ZoneId.systemDefault()).toInstant());
            Date fin = Date.from(LocalDate.parse(end).atStartOfDay(ZoneId.systemDefault()).toInstant());
            Connection con = (Connection) em.unwrap(Connection.class);
            URL in = this.getClass().getResource("AgendaMedico.jasper");
            String dir = dirArchivo();
            JasperReport report = (JasperReport) JRLoader.loadObject(in);
            Map parametros = new HashMap();
            parametros.put("cAtendidas", atendidas.toString());
            parametros.put("cAusentes", ausentes.toString());
            parametros.put("cCanceladas", canceladas.toString());
            parametros.put("cProgramadas", programadas.toString());
            parametros.put("EspacialidadMedico", med.getMedEspId().getEspNombre());
            parametros.put("fin", fin);
            parametros.put("FolioMedico", med.getMedFolio());
            parametros.put("inicio", inicio);
            parametros.put("med", med.getMedId());
            parametros.put("NombreMedico", med.getMedUsuId().getUsuNombre() + " " + med.getMedUsuId().getUsuPapellido());

            JasperPrint print = JasperFillManager.fillReport(report, parametros, con);
            JasperExportManager.exportReportToXmlFile(print, dir + "AgendaMedico.xml", true);

            Base64.Encoder encode = Base64.getEncoder();
            File file = new File(dir + "AgendaMedico.xml");
            byte[] fileArray = new byte[(int) file.length()];
            String encodedFile = "";
            InputStream inputStream;
            inputStream = new FileInputStream(file);
            inputStream.read(fileArray);
            inputStream.close();
            encodedFile = encode.encodeToString(fileArray);
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Reporte", encodedFile);
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio con reporte", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al generar el reporte.", "reporteMed " + ex.getMessage());

        }
    }

    public Respuesta reporPaciente(Long id) {
        try {
            Paciente paciente = em.find(Paciente.class, id);
            Connection con = (Connection) em.unwrap(Connection.class);
            URL in = this.getClass().getResource("ExpedientePaciente.jasper");
            String dir = dirArchivo();
            JasperReport report = (JasperReport) JRLoader.loadObject(in);
            Map parametros = new HashMap();
            parametros.put("idPaciente", paciente.getPacId());
            JasperPrint print = JasperFillManager.fillReport(report, parametros, con);
            JasperExportManager.exportReportToXmlFile(print, dir + "ExpedientePaciente.xml", true);

            Base64.Encoder encode = Base64.getEncoder();
            File file = new File(dir + "ExpedientePaciente.xml");
            byte[] fileArray = new byte[(int) file.length()];
            String encodedFile = "";
            InputStream inputStream;
            inputStream = new FileInputStream(file);
            inputStream.read(fileArray);
            inputStream.close();
            encodedFile = encode.encodeToString(fileArray);
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Reporte", encodedFile);
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio con reporte", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al generar el reporte.", "reporteMed " + ex.getMessage());

        }
    }

    public Respuesta reporEspecilaidad(String id, String ini, String end) {
        try {
            String ids = "%";
            if (!id.equals("%")) {
                Especialidad especialidad = em.find(Especialidad.class, Long.parseLong(id));
                ids = especialidad.getEspId().toString();
            }

            Date inicio = Date.from(LocalDate.parse(ini).atStartOfDay(ZoneId.systemDefault()).toInstant());
            Date fin = Date.from(LocalDate.parse(end).atStartOfDay(ZoneId.systemDefault()).toInstant());
            Connection con = (Connection) em.unwrap(Connection.class);
            URL in = this.getClass().getResource("Especialidad.jasper");
            String dir = dirArchivo();
            JasperReport report = (JasperReport) JRLoader.loadObject(in);
            Map parametros = new HashMap();
            parametros.put("fecInicio", inicio);
            parametros.put("fecFin", fin);
            parametros.put("id", ids);
            //parametros.put("id", "%");
            JasperPrint print = JasperFillManager.fillReport(report, parametros, con);
            JasperExportManager.exportReportToXmlFile(print, dir + "Especialidad.xml", true);
            Base64.Encoder encode = Base64.getEncoder();
            File file = new File(dir + "Especialidad.xml");
            byte[] fileArray = new byte[(int) file.length()];
            String encodedFile = "";
            InputStream inputStream;
            inputStream = new FileInputStream(file);
            inputStream.read(fileArray);
            inputStream.close();
            encodedFile = encode.encodeToString(fileArray);
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Reporte", encodedFile);
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio con reporte", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al generar el reporte.", "reporteMed " + ex.getMessage());

        }
    }
}

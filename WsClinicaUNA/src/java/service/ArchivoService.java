/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import model.ArchivoDto;
import model.Archivos;
import model.Expediente;
import model.ExpedienteDto;
import util.CodigoRespuesta;
import util.ConverArchivo;
import util.Respuesta;

@Stateless
public class ArchivoService {
    private static final Logger LOG = Logger.getLogger(ArchivoService.class.getName());
    
    @PersistenceContext(unitName = "WsClinicaUNAPU")
    private EntityManager em; 
    @EJB
    ExpedienteService expedienteService;

    public Respuesta guardarArchivos(ArchivoDto archivoDto) {
        try {
            Archivos archivo;
            if (archivoDto.getId()!= null && archivoDto.getId()> 0) {
                archivo = em.find(Archivos.class, archivoDto.getId());
                if (archivo == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontró el archivo a modificar.", "guardarArchivos NoResultException");
                }
                archivo.actualizar(archivoDto);
                archivo = em.merge(archivo);
            } else {
                archivo = new Archivos(archivoDto);
                if(archivoDto.getTipo().equals("PDF")){
                    String ruta = ConverArchivo.stringToFile(archivoDto.getArchivo(), archivoDto);
                    archivo.setArcRuta(ruta);
                }else{
                    String ruta = ConverArchivo.guardarImagen(archivoDto.getArchivo(), archivoDto);
                    archivo.setArcRuta(ruta);
                }
                em.persist(archivo);
                Expediente ex = em.find(Expediente.class, archivoDto.getExpediente().getId());
                ex.getArchivosList().add(archivo);
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Archivos", new ArchivoDto(archivo));
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al guardar el archivo.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al guardar el archivo.", "guardarArchivos " + ex.getMessage());
        }
    }
    
    public Respuesta getArchivos(Long exp) {
        try {
            Expediente ex = em.find(Expediente.class, exp);
            Query qryArchivos = em.createNamedQuery("Archivos.findByExpId", Archivos.class);
            qryArchivos.setParameter("arcExpId", ex);
            List<Archivos> archivos = qryArchivos.getResultList();
            List<ArchivoDto> archivosDto = new ArrayList<>();
            
            for (Archivos archivo : archivos) {
                archivosDto.add(new ArchivoDto(archivo));
            }

            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Archivos", archivosDto);
        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existen archivos con los criterios ingresados.", "getArchivoss NoResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el archivo.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el archivo.", "getArchivos " + ex.getMessage());
        }
    }
    
    public Respuesta getArchivo(Long archId) {
        try {
            Archivos archivo = em.find(Archivos.class, archId);
            ArchivoDto archivoDto = new ArchivoDto(archivo);
            String path = "";
            if(archivo.getArcTipo().equals("PDF")){
                path = ConverArchivo.fileToString(archivo.getArcRuta());
            }else{
                path = ConverArchivo.getImage(archivo.getArcRuta());
            }
            archivoDto.setArchivo(path);
            
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Archivo", archivoDto);
        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existen archivos con los criterios ingresados.", "getArchivoss NoResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el archivo.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el archivo.", "getArchivos " + ex.getMessage());
        }
    }
    
    public Respuesta eliminarArchivos(Long id) {
        try {
            Archivos archivo;
            if (id != null && id > 0) {
                archivo = em.find(Archivos.class, id);
                if (archivo == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encrontró el archivo a eliminar.", "eliminarUsuario NoResultException");
                }
                em.remove(archivo);
            } else {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "Debe cargar el archivo a eliminar.", "eliminarArchivos NoResultException");
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "");
        } catch (Exception ex) {
            if (ex.getCause() != null && ex.getCause().getCause().getClass() == SQLIntegrityConstraintViolationException.class) {
                return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "No se puede eliminar el archivo porque tiene relaciones con otros registros.", "eliminarArchivos " + ex.getMessage());
            }
            LOG.log(Level.SEVERE, "Ocurrio un error al guardar el archivo.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al eliminar el archivo.", "eliminarArchivos " + ex.getMessage());
        }
    }
}

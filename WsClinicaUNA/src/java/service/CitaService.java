/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import model.Cita;
import model.CitaDto;
import model.Medico;
import model.MedicoDto;
import model.Paciente;
import model.PacienteDto;
import util.CodigoRespuesta;
import util.Respuesta;

/**
 *
 * @author liedu
 */
@Stateless
@LocalBean
public class CitaService {

    private static final Logger LOG = Logger.getLogger(PacienteService.class.getName());

    @PersistenceContext(unitName = "WsClinicaUNAPU")
    private EntityManager em;

    public Respuesta guardarCita(CitaDto citaDto) {
        try {
            Cita cita;
            Medico medico = em.find(Medico.class, citaDto.getCitMedId().getMedId());
            Paciente paciente = em.find(Paciente.class, citaDto.getPaciente().getPacId());
            if (citaDto.getCitId() != null && citaDto.getCitId() > 0) {
                cita = em.find(Cita.class, citaDto.getCitId());
                if (cita == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encrontró el cita a modificar.", "guardarCita NoResultException");
                }
                cita.actualizar(citaDto);
                cita = em.merge(cita);
            } else {
                cita = new Cita(citaDto);
                cita.setCitMedId(medico);
                cita.setCitPacId(paciente);
                em.persist(cita);
            }
            em.flush();
            CitaDto cDto = new CitaDto(cita);
            cDto.setPaciente(new PacienteDto(paciente));
            cDto.setCitMedId(new MedicoDto(medico));
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Cita", cDto);
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al guardar el cita.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al guardar el cita.", "guardarCita " + ex.getMessage());
        }
    }

    public Respuesta allCitas(Long id, String CitFecha) {
        try {
            CitaDto cDto;
            Date dia = Date.from(LocalDate.parse(CitFecha).atStartOfDay(ZoneId.systemDefault()).toInstant());
            Query query = em.createNamedQuery("Cita.findByMedicoCita", Cita.class);
            query.setParameter("idMedico", id);
            query.setParameter("fechaCita", dia);
            List<Cita> citas = query.getResultList();
            List<CitaDto> citasDto = new ArrayList<>();
            for (Cita cita : citas) {
                cDto = new CitaDto(cita);
                cDto.setCitMedId(new MedicoDto(cita.getCitMedId()));
                cDto.setPaciente(new PacienteDto(cita.getCitPacId()));
                citasDto.add(cDto);
            }

            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Citas", citasDto);
        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existen citas con los criterios ingresados.", "getcitas NoResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el citas.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar la cita.", "citas " + ex.getMessage());
        }
    }

    public Respuesta eliminarCita(Long citId) {
        try {
            Cita cita;
            if (citId != null && citId > 0) {
                cita = em.find(Cita.class, citId);
                if (cita == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontró  la cita a eliminar.", "eliminarCita NoResultException");
                }
                em.remove(cita);
            } else {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "Debe cargar  la cita a eliminar.", "eliminarCita NoResultException");
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "Eliminacion Exitosa", "");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al eliminar la cita.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al eliminar la cita.", "eliminarMedico " + ex.getMessage());
        }
    }
    
        public Respuesta getCitas(String med, String CitFecha, String paci) {
        try {
            CitaDto cDto;
            Date dia = Date.from(LocalDate.parse(CitFecha).atStartOfDay(ZoneId.systemDefault()).toInstant());
            Query query = em.createNamedQuery("Cita.findByMedPacFec", Cita.class);
            query.setParameter("medName", med);
            query.setParameter("paciName", paci);
            query.setParameter("fecha", dia);
            List<Cita> citas = query.getResultList();
            List<CitaDto> citasDto = new ArrayList<>();
            for (Cita cita : citas) {
                cDto = new CitaDto(cita);
                cDto.setCitMedId(new MedicoDto(cita.getCitMedId()));
                cDto.setPaciente(new PacienteDto(cita.getCitPacId()));
                citasDto.add(cDto);
            }

            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Citas", citasDto);
        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existen citas con los criterios ingresados.", "getcitas NoResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el citas.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar la cita.", "citas " + ex.getMessage());
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import model.ExpedienteDto;
import model.Paciente;
import model.PacienteDto;
import util.CodigoRespuesta;
import util.Respuesta;

/**
 *
 * @author Susana
 */
@Stateless
@LocalBean
public class PacienteService {
    
    private static final Logger LOG = Logger.getLogger(PacienteService.class.getName());
    
    @PersistenceContext(unitName = "WsClinicaUNAPU")
    private EntityManager em; 
    @EJB
    ExpedienteService expedienteService;

    public Respuesta guardarPaciente(PacienteDto pacienteDto) {
        try {
            Paciente paciente;
            if (pacienteDto.getPacId()!= null && pacienteDto.getPacId()> 0) {
                paciente = em.find(Paciente.class, pacienteDto.getPacId());
                if (paciente == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontró el paciente a modificar.", "guardarPaciente NoResultException");
                }
                paciente.actualizar(pacienteDto);
                paciente = em.merge(paciente);
            } else {
                paciente = new Paciente(pacienteDto);
                em.persist(paciente);
                ExpedienteDto exp = new ExpedienteDto();
                PacienteDto pac = new PacienteDto(paciente);
                exp.setPaciente(pac);
                expedienteService.guardarExpediente(exp);
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Paciente", new PacienteDto(paciente));
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al guardar el paciente.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al guardar el paciente.", "guardarPaciente " + ex.getMessage());
        }
    }
    
    public Respuesta getPacientes(String cedula, String nombre, String apellidos) {
        try {
            Query qryPaciente = em.createNamedQuery("Paciente.findByCedulaNombreApellidos", Paciente.class);
            qryPaciente.setParameter("cedula","%"+ cedula + "%");
            qryPaciente.setParameter("nombre","%"+ nombre + "%");
            qryPaciente.setParameter("apellidos","%"+ apellidos +"%");
            List<Paciente> pacientes = qryPaciente.getResultList();
            List<PacienteDto> pacientesDto = new ArrayList<>();
            
            for (Paciente paciente : pacientes) {
                pacientesDto.add(new PacienteDto(paciente));
            }

            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Pacientes", pacientesDto);
        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existen pacientes con los criterios ingresados.", "getPacientes NoResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el paciente.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el paciente.", "getPaciente " + ex.getMessage());
        }
    }
    
    public Respuesta eliminarPaciente(Long id) {
        try {
            Paciente paciente;
            Long idExp;
            if (id != null && id > 0) {
                paciente = em.find(Paciente.class, id);
                if (paciente == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encrontró el paciente a eliminar.", "eliminarUsuario NoResultException");
                }
                idExp = paciente.getPacExpId().getExpId();
                em.remove(paciente);
            } else {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "Debe cargar el paciente a eliminar.", "eliminarPaciente NoResultException");
            }
            em.flush();
            expedienteService.eliminarExpediente(idExp);
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "");
        } catch (Exception ex) {
            if (ex.getCause() != null && ex.getCause().getCause().getClass() == SQLIntegrityConstraintViolationException.class) {
                return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "No se puede eliminar el paciente porque tiene relaciones con otros registros.", "eliminarPaciente " + ex.getMessage());
            }
            LOG.log(Level.SEVERE, "Ocurrio un error al guardar el paciente.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al eliminar el paciente.", "eliminarPaciente " + ex.getMessage());
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import model.Usuario;
import model.UsuarioDto;
import util.CodigoRespuesta;
import util.Respuesta;

/**
 *
 * @author Dani
 */
@Stateless
@LocalBean
public class UsuarioService 
{
    private static final Logger LOG = Logger.getLogger(UsuarioService.class.getName());
    
    @PersistenceContext(unitName = "WsClinicaUNAPU")
    private EntityManager em;
    
    public Respuesta getUsuario(String usuario,String contra)
    {
        try
        {
            Query qryUsuario = em.createNamedQuery("Usuario.findByUsuarioContrasena",Usuario.class);
            qryUsuario.setParameter("usuUsuario",usuario);
            qryUsuario.setParameter("usuContrasena",contra); 
            
            return new Respuesta(true,CodigoRespuesta.CORRECTO,"","","Usuario", new UsuarioDto((Usuario) qryUsuario.getSingleResult()));
        }
        catch (NoResultException ex) 
        {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existe un usuario con el código ingresado.", "getUsuario");
        }
        catch (NonUniqueResultException ex) 
        {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el usuario.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el usuario.", "getUsuario NonUniqueResultException");
        } 
        catch(Exception ex)
        {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el usuario.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO,"Ocurrio un error al consultar el usuario","getUsuario "+ex.getMessage());
        }
    }
    
    public Respuesta guardarUsuario(UsuarioDto usuarioDto)
    {
        try 
        {
            Usuario usuario;
            if (usuarioDto.getUsuId()!= null && usuarioDto.getUsuId()> 0) 
            {
                usuario = em.find(Usuario.class, usuarioDto.getUsuId());
                if (usuario == null) 
                {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontró el usuario a modificar.", "guardarUsuario NoResultException");
                }
                usuario.actualizar(usuarioDto);
                usuario = em.merge(usuario);
            } 
            else 
            {
               usuario = new Usuario(usuarioDto);
               usuario.setUsuEstado("I");
               em.persist(usuario);
            //   em.flush();
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Usuario", new UsuarioDto(usuario));
        } 
        catch (Exception ex) 
        {
            LOG.log(Level.SEVERE, "Ocurrio un error al guardar el usuario.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al guardar el usuario.", "guardarUsuario " + ex.getMessage());
        }
    }
    public Respuesta recuperarUsuario(String correo, String contrasena)
    {
        try
        {
            Usuario usuario;
            Query qryUsuario=em.createNamedQuery("Usuario.findByUsuCorreo", Usuario.class);
            qryUsuario.setParameter("usuCorreo",correo);
            
            usuario=(Usuario)qryUsuario.getSingleResult();
            if(usuario.getUsuId()!=null && usuario.getUsuId()>0)
            {
                usuario.setUsuContrasena(contrasena);
                em.merge(usuario);
                //em.refresh(usuario);
                em.flush();
                
            }
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Usuario", new UsuarioDto((Usuario) qryUsuario.getSingleResult()));
            
        }
        catch(Exception ex)
        {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el usuario.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO,"Ocurrio un error al concultar al usuario","recuperarUsuario"+ex.getMessage());
        }
    }
    public Respuesta activarUsuario(Long usuId)
    {
        try
        {
            Usuario usu = new Usuario();
            if(usuId != null && usuId > 0)
            {
                usu=em.find(Usuario.class,usuId);
                usu.setUsuEstado("A");
                usu=em.merge(usu);
                em.flush();
                
            }
            return new Respuesta(true,CodigoRespuesta.CORRECTO,"","","Usuario",new UsuarioDto(usu));
        }
        catch(Exception ex)
        {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el usuario.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el usuario.", "activarUsuario " + ex.getMessage());
        }
    }
    public Respuesta eliminarUsuario(Long usuId)
    {
        try
        {
            Usuario usuario;
            if(usuId != null && usuId > 0)
            {
                usuario = em.find(Usuario.class, usuId);
                if(usuario == null)
                {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO,"No se encontro el usuario a eliminar","Eliminar usuario");
                }
                em.remove(usuario);
            }
            else
            {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "Debe cargar el usuario a eliminar.", "eliminarUsuario");
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "Usuario eliminado", "");
        }
        catch(Exception ex)
        {
            if (ex.getCause() != null && ex.getCause().getCause().getClass() == SQLIntegrityConstraintViolationException.class) {
                return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "No se puede eliminar el usuario porque tiene relaciones con otros registros.", "eliminarUsuario " + ex.getMessage());
            }
            LOG.log(Level.SEVERE, "Ocurrio un error al eliminar al usuario.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al eliminar el usuario.", "eliminarUsuario " + ex.getMessage());
        }
    }
    
    public Respuesta getUsuarios(String nombre, String cedula,String apellido)
    {
        try
        {
            Query qryUsuario = em.createNamedQuery("Usuario.findByNombreCedulaApellidos",Usuario.class);
            qryUsuario.setParameter("nombre","%"+nombre.toUpperCase()+"%");
            qryUsuario.setParameter("cedula","%"+cedula.toUpperCase()+"%");
            qryUsuario.setParameter("apellido","%"+apellido.toUpperCase()+"%");
            
            List<Usuario> usuarios = qryUsuario.getResultList();
            List<UsuarioDto> usuariosDto = new ArrayList<>();
            for (Usuario usuario : usuarios) 
            {
                usuariosDto.add(new UsuarioDto(usuario));
            }
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Usuarios", usuariosDto);
        }
        catch (NoResultException ex) 
        {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existen usuarios con los criterios ingresados.", "getUsuarios NoResultException");
        }
        catch(Exception ex)
        {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el usuario.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el usuario.", "getUsuario " + ex.getMessage());
        }
    }
    public Respuesta validarUsuario(String usuario)
    {
        try 
        {
            Query qryUsuario = em.createNamedQuery("Usuario.findByUsuUsuario",Usuario.class);
            qryUsuario.setParameter("usuUsuario",usuario);
            return new Respuesta(true,CodigoRespuesta.CORRECTO,"","","Usuario", new UsuarioDto((Usuario) qryUsuario.getSingleResult()));
        } 
        catch (NoResultException ex) 
        {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existe un usuario con el código ingresado.", "validarUsuario");
        }
        catch (NonUniqueResultException ex) 
        {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el usuario.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el usuario.", "validarUsuario NonUniqueResultException");
        } 
        catch(Exception ex)
        {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el usuario.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO,"Ocurrio un error al consultar el usuario","validarUsuario "+ex.getMessage());
        }
    }

    public Respuesta getUsuId(Long usuId) {
        try 
        {
            Usuario usuario = em.find(Usuario.class,usuId);
            return new Respuesta(true,CodigoRespuesta.CORRECTO,"","","Usuario", new UsuarioDto(usuario));
        } 
        catch (NoResultException ex) 
        {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existe un usuario con el código ingresado.", "validarUsuario");
        }
        catch (NonUniqueResultException ex) 
        {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el usuario.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el usuario.", "validarUsuario NonUniqueResultException");
        } 
        catch(Exception ex)
        {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el usuario.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO,"Ocurrio un error al consultar el usuario","validarUsuario "+ex.getMessage());
        }
    }
}

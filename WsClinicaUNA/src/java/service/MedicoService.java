/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import model.Medico;
import model.MedicoDto;
import model.Usuario;
import util.CodigoRespuesta;
import util.Respuesta;

/**
 *
 * @author Dani
 */
@Stateless
@LocalBean
public class MedicoService 
{
    private static final Logger LOG = Logger.getLogger(MedicoService.class.getName());
    @PersistenceContext(unitName = "WsClinicaUNAPU")
    private EntityManager em;
    
    public Respuesta guardarMedico(MedicoDto medicoDto)
    {
        try
        {
            Medico medico;
            if (medicoDto.getMedId()!= null && medicoDto.getMedId()> 0) 
            {
                medico = em.find(Medico.class, medicoDto.getMedId());
                if (medico == null) 
                {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontró el medico a modificar.", "guardarMedico NoResultException");
                }
                medico.actualizar(medicoDto);
                medico = em.merge(medico);
            } 
            else 
            {
               medico = new Medico(medicoDto);
               em.persist(medico);
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Medico", new MedicoDto(medico));
        }
        catch(Exception ex)
        {
            LOG.log(Level.SEVERE, "Ocurrio un error al guardar el medico.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al guardar el medico.", "guardarMedico " + ex.getMessage());
        }
    }
    public Respuesta eliminarMedico(Long medId)
    {
        try
        {
            Medico medico;
            if (medId != null && medId > 0) 
            {
                medico = em.find(Medico.class, medId);
                if (medico == null) 
                {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontró el medico a eliminar.", "eliminarMedico NoResultException");
                }
                em.remove(medico);
            } else {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "Debe cargar el medico a eliminar.", "eliminarMedico NoResultException");
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "");
        }
        catch(Exception ex)
        {
            LOG.log(Level.SEVERE, "Ocurrio un error al eliminar al medico.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al eliminar el medico.", "eliminarMedico " + ex.getMessage());
        }
        
    }
    public Respuesta getMedico(String folio,String carne,String codigo) 
    {
        try
        {
            Query qryMedico = em.createNamedQuery("Medico.findByFolioCarneCod",Medico.class);
            qryMedico.setParameter("folio","%"+folio.toUpperCase()+"%");
            qryMedico.setParameter("carne","%"+carne.toUpperCase()+"%");
            qryMedico.setParameter("codigo","%"+codigo.toUpperCase()+"%");
            
            List<Medico> medicos = qryMedico.getResultList();
            List<MedicoDto> medicosDto = new ArrayList<>();
            for (Medico medico : medicos) 
            {
                medicosDto.add(new MedicoDto(medico));
            }
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Medicos", medicosDto);
        }
        catch (NoResultException nex) 
        {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existen medicos con los criterios ingresados.", "getMedicos NoResultException");
        }
        catch(Exception ex)
        {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el medico.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el medico.", "getMedico " + ex.getMessage());
        }
    }
    public Respuesta getEspecialidad(Long EspId)
    {
        try
        {
            Query qryEsp = em.createNamedQuery("Medico.findByEspecialidad",Medico.class);
            qryEsp.setParameter("EspId",EspId); //EspId
            
            List<Medico> med = qryEsp.getResultList();
            List<MedicoDto> medDto = new ArrayList<>();
            for (Medico medico : med) 
            {
                medDto.add(new MedicoDto(medico));
            }
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "EspecialidadM", medDto);
        }
        catch(Exception ex)
        {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar la especialidad.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar la especialidad.", "getMedico " + ex.getMessage());
        }
    }

    public Respuesta getMedico(Long usu) {
        try
        {
            //Usuario usuario = em.find(Usuario.class, usu);
            Query qryMedico = em.createNamedQuery("Medico.findByUsuario",Medico.class);
            qryMedico.setParameter("medUsuId",usu);
            
            Medico medico = (Medico) qryMedico.getSingleResult();
            
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Medico", new MedicoDto(medico));
        }
        catch (NoResultException nex) 
        {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existen medicos con los criterios ingresados.", "getMedicos NoResultException");
        }
        catch(Exception ex)
        {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el medico.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el medico.", "getMedico " + ex.getMessage());
        }
    }
       
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import model.Expediente;
import model.ExpedienteDto;
import model.Paciente;
import util.CodigoRespuesta;
import util.Respuesta;

/**
 *
 * @author Susana
 */
@Stateless
@LocalBean
public class ExpedienteService {

    private static final Logger LOG = Logger.getLogger(ExpedienteService.class.getName());

    @PersistenceContext(unitName = "WsClinicaUNAPU")
    private EntityManager em;

    public Respuesta guardarExpediente(ExpedienteDto expedienteDto) {
        try {
            Expediente expediente;// = new Expediente(expedienteDto);
            Paciente paciente = em.find(Paciente.class, expedienteDto.getPaciente().getPacId());
            if (expedienteDto.getId() != null && expedienteDto.getId() > 0) {
                expediente = em.find(Expediente.class, expedienteDto.getId());
                if (expediente == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontró el expediente a modificar.", "guardarExpediente NoResultException");
                }
                expediente.actualizar(expedienteDto);
                expediente = em.merge(expediente);
            } else {
                expediente = new Expediente(expedienteDto);
                List<Paciente> pacientes = Arrays.asList(paciente);
                expediente.setPacienteList(pacientes);
                em.persist(expediente);
                paciente.setPacExpId(expediente);
                em.merge(paciente);

            }
            em.flush();
            expedienteDto = new ExpedienteDto(expediente);
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Expediente", expedienteDto);
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al guardar el expediente.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al guardar el expediente.", "guardarExpediente " + ex.getMessage());
        }
    }

    public Respuesta getExpedientes(Long pacId) {
        try {
            Paciente paciente = em.find(Paciente.class, pacId);

            Query qryExpediente = em.createNamedQuery("Expediente.findByPacId", Expediente.class);
            qryExpediente.setParameter("pacienteList", paciente);
            Expediente expediente = (Expediente) qryExpediente.getSingleResult();
            ExpedienteDto expedienteDto = new ExpedienteDto(expediente);

            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "expediente", expedienteDto);
        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "El paciente no tiene un expediente.", "getExpedientes NoResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el expediente.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el expediente.", "getExpediente " + ex.getMessage());
        }
    }

    public Respuesta eliminarExpediente(Long id) {
        try {
            Expediente expediente;
            if (id != null && id > 0) {
                expediente = em.find(Expediente.class, id);
                if (expediente == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encrontró el expediente a eliminar.", "eliminarUsuario NoResultException");
                }
                em.remove(expediente);
            } else {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "Debe cargar el expediente a eliminar.", "eliminarExpediente NoResultException");
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "");
        } catch (Exception ex) {
            if (ex.getCause() != null && ex.getCause().getCause().getClass() == SQLIntegrityConstraintViolationException.class) {
                return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "No se puede eliminar el expediente porque tiene relaciones con otros registros.", "eliminarExpediente " + ex.getMessage());
            }
            LOG.log(Level.SEVERE, "Ocurrio un error al guardar el expediente.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al eliminar el expediente.", "eliminarExpediente " + ex.getMessage());
        }
    }
}

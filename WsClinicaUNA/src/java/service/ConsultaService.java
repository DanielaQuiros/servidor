/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import model.Consulta;
import model.ConsultaDto;
import util.CodigoRespuesta;
import util.Respuesta;

/**
 *
 * @author Dani
 */
@Stateless
@LocalBean
public class ConsultaService 
{
    private static final Logger LOG = Logger.getLogger(ConsultaService.class.getName());
    
    @PersistenceContext(unitName = "WsClinicaUNAPU")
    private EntityManager em;
    
    public Respuesta guardarConsulta(ConsultaDto consultaDto)
    {
        try
        {
            Consulta consulta;
            if (consultaDto.getConId()!= null && consultaDto.getConId()> 0) 
            {
                consulta = em.find(Consulta.class, consultaDto.getConId());
                if (consulta == null) 
                {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontró el consulta a modificar.", "guardarConsulta NoResultException");
                }
                consulta.actualizar(consultaDto);
                consulta = em.merge(consulta);
            } 
            else 
            {
               consulta = new Consulta(consultaDto);
               em.persist(consulta);
               em.flush();
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Consulta", new ConsultaDto(consulta));
        }
        catch(Exception ex)
        {
            LOG.log(Level.SEVERE, "Ocurrio un error al guardar la consulta.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al guardar la consulta.", "guardarConsulta NonUniqueResultException");
        }
    }
    public Respuesta getConsulta(Long citId)
    {
        try
        {
            Query qryConsulta = em.createNamedQuery("Consulta.findByCita",Consulta.class);
            qryConsulta.setParameter("citId",citId);

            return new Respuesta(true,CodigoRespuesta.CORRECTO,"","","Consultas",new ConsultaDto((Consulta)qryConsulta.getSingleResult()));
        }
        catch(Exception ex)
        {
            LOG.log(Level.SEVERE, "Ocurrio un error al obtener la consulta.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al obtener la consulta.", "getConsulta");
        }
    }
    public Respuesta eliminarConsulta(Long conId)
    {
        try
        {
            Consulta consulta;
            if(conId != null && conId > 0)
            {
                consulta = em.find(Consulta.class, conId);
                if(consulta == null)
                {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO,"No se encontro la consulta a eliminar","Eliminar consulta");
                }
                em.remove(consulta);
            }
            else
            {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "Debe cargar la consulta a eliminar.", "eliminarConsulta");
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "Consulta eliminada", "");
        }
        catch(Exception ex)
        {
           LOG.log(Level.SEVERE, "Ocurrio un error al eliminar la consulta.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al eliminar la consulta.", "getConsulta"); 
        }
    }

    public Respuesta getConsultas(Long pacId) {
        try
        {
            Query qryConsulta = em.createNamedQuery("Consulta.findByExpediente",Consulta.class);
            qryConsulta.setParameter("expId",pacId);
            List<Consulta> consulta = qryConsulta.getResultList();
            List<ConsultaDto> consultaDto = new ArrayList<>();
            for (Consulta con : consulta) 
            {
                consultaDto.add(new ConsultaDto(con));
            }
            return new Respuesta(true,CodigoRespuesta.CORRECTO,"","","Consultas",consultaDto);
        }
        catch(Exception ex)
        {
            LOG.log(Level.SEVERE, "Ocurrio un error al obtener la consulta.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al obtener la consulta.", "getConsulta");
        }
    }
            
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import model.Especialidad;
import model.EspecialidadDto;
import util.CodigoRespuesta;
import util.Respuesta;

/**
 *
 * @author Dani
 */
@Stateless
@LocalBean
public class EspecialidadService 
{
    private static final Logger LOG = Logger.getLogger(EspecialidadService.class.getName());
    
    @PersistenceContext(unitName = "WsClinicaUNAPU")
    private EntityManager em;
    
    public Respuesta guardarEspecialidad(EspecialidadDto especialidadDto)
    {
        try
        {
            Especialidad especialidad;
            if (especialidadDto.getEspId()!= null && especialidadDto.getEspId()> 0) 
            {
                especialidad = em.find(Especialidad.class, especialidadDto.getEspId());
                if (especialidad == null) 
                {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontró el especialidad a modificar.", "guardarEspecialidad NoResultException");
                }
                especialidad.actualizar(especialidadDto);
                especialidad = em.merge(especialidad);
            } 
            else 
            {
               especialidad = new Especialidad(especialidadDto);
               em.persist(especialidad);
               em.flush();
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Especialidad", new EspecialidadDto(especialidad));
        }
        catch(Exception ex)
        {
            LOG.log(Level.SEVERE, "Ocurrio un error al guardar el especialidad.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al guardar el especialidad.", "guardarEspecialidad " + ex.getMessage());
        }
    }
    public Respuesta getEspecialidad(String nombre)
    {
        try
        {
            Query qryEsp = em.createNamedQuery("Especialidad.findByEspNombre",Especialidad.class);
            qryEsp.setParameter("espNombre",nombre);
            List<Especialidad> especialidades = qryEsp.getResultList();
            List<EspecialidadDto> espDto = new ArrayList<>();
            for (Especialidad especialidad : especialidades) 
            {
                espDto.add(new EspecialidadDto(especialidad));
            }
            
            return new Respuesta(true,CodigoRespuesta.CORRECTO,"","","Especialidad",espDto );
        }
        catch(Exception ex)
        {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el especialidad.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el especialidad.", "getEspecialidad " + ex.getMessage());
        }
    }
    public Respuesta eliminarEspecialidad(Long EspId)
    {
        try
        {
            Especialidad especialidad;
            if(EspId != null && EspId > 0)
            {
                especialidad = em.find(Especialidad.class, EspId);
                if(especialidad == null)
                {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO,"No se encontro la especialidad a eliminar","Eliminar especialidad");
                }
                em.remove(especialidad);
            }
            else
            {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "Debe cargar la especialidad a eliminar.", "eliminarEspecialidad");
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "Especialidad eliminada", "");
        }
        catch(Exception ex)
        {
            if(ex.getCause() != null && ex.getCause().getCause().getClass() == SQLIntegrityConstraintViolationException.class)
            {
                return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "No se puede eliminar la especialidad porque posee relaciones con otros registros.", "eliminarEspecialidad " + ex.getMessage());
            }
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar la especialidad.", ex);
            return new Respuesta(false,CodigoRespuesta.ERROR_INTERNO,"Ocurrio un error al eliminar la especialidad","");
        }
    }
    public Respuesta getEspecialidades()
    {
        try
        {
            Query qryEsp = em.createNamedQuery("Especialidad.findAll",Especialidad.class);
            List<Especialidad> especialidades = qryEsp.getResultList();
            List<EspecialidadDto> especialidadDto = new ArrayList<>();
            for (Especialidad especialidad : especialidades) 
            {
                especialidadDto.add(new EspecialidadDto(especialidad));
            }
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Especialidades", especialidadDto);
        }
        catch(Exception ex)
        {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el especialidad.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el especialidad.", "getEspecialidades " + ex.getMessage());
        }
    }
    public Respuesta getEsp()
    {
        try
        {
            Query qryEsp = em.createNamedQuery("Especialidad.findByEspMed",Especialidad.class);
           // qryEsp.setParameter("espNombre",nombre);
            List<Especialidad> especialidades = qryEsp.getResultList();
            List<EspecialidadDto> espDto = new ArrayList<>();
            for (Especialidad especialidad : especialidades) 
            {
                espDto.add(new EspecialidadDto(especialidad));
            }
            
            return new Respuesta(true,CodigoRespuesta.CORRECTO,"","","Especialidad",espDto );
        }
        catch(Exception ex)
        {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el especialidad.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el especialidad.", "getEspecialidad " + ex.getMessage());
        }
    }
 
}
